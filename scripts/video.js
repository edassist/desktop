//really compilcated because of subtitle synchronization

var timestamps = null;
var secondsTimestamps = [];
var playbackStopped; //true if playback was stopped in between
var playbackSlowed;//true if the playback was slowed
var playbackRate = 1;

//qt webchannel setup
window.onload = function() {
  new QWebChannel(qt.webChannelTransport, function(channel) {
    // now you retrieve your object
    window.qtCpp = channel.objects.channelObject;
    qtCpp.sendingDataPoints.connect(function(data) {
        timestamps = data;
        secondsTimestamps = window.timestamps.map(timeStringToSeconds)
        window.target = secondsTimestamps[0];//the next time in seconds where the subtitles need to be changed.
        playbackStopped = true;
    });
    qtCpp.seekTo.connect(function(timeString){
        var seconds = timeStringToSeconds(timeString);
        player.seekTo(seconds,true);
    });
  });
};

//youtube stuff. get it from youtbue iframe api site
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var player;

function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    height: '360',
    width: '640',
    videoId: 'bfvUtSumLcE',
    events: {
      'onStateChange': onPlayerStateChange,
      'onPlaybackRateChange' : onPlayerPlaybackRateChange
    }
  });
};

function onPlayerPlaybackRateChange(event){
    var oldPlaybackRate = playbackRate;
    playbackRate = player.getPlaybackRate();
    if (oldPlaybackRate > playbackRate) {
        playbackSlowed = true;
        //console.log("Playback slowed");
    }
}

function onPlayerStateChange(event) {
  if (timestamps != null && event.data === YT.PlayerState.PLAYING){
    if(playbackStopped){
      if (player.getCurrentTime() <= target){
          playbackStopped = false;
          //console.log("Setting playbackStopped false in onPlayerStateChange");
          //console.log("Setting timeout from onPlayerStateChange ",target-player.getCurrentTime());
          setTimeout(changeSubtitles, ((target-player.getCurrentTime())/playbackRate)*1000);
      }
    }
  }
};

function timeStringToSeconds(timeString){
    //changes timeString to integer representing the time in seconds
    var timeStrings = timeString.split(':');
    var timeSec = parseInt(timeStrings[0])*3600;
    timeSec += parseInt(timeStrings[1])*60;
    timeSec += parseInt(timeStrings[2].split('.')[0]);
    return timeSec;
}

function emitChangeSubtitles(index){
    //calls the emitChangeSubtitles function of the qtCpp object

    qtCpp.emitChangeSubtitles(timestamps[index]);
    //console.log("emitted ",timestamps[index]);
    if (index !== secondsTimestamps.length-1){ //otherwise if at end of list do nothing
        target = secondsTimestamps[index+1];
        //console.log("Setting timeout from emitChangeSubtitles ",target-player.getCurrentTime());
        setTimeout(changeSubtitles, ((target-player.getCurrentTime())/playbackRate)*1000);
    }
}

function changeSubtitles(){
    //calls the emitChangeSubtitles() whenever needed. This function is always called through a timer
    var currentTime = player.getCurrentTime();

    //when the viewer stops the playback in between or viewer seeks back
    if (Math.ceil(currentTime) < target){
        var currentTimeCeil = Math.ceil(currentTime);

        //when the viewer slows in between
        if (playbackSlowed){
            playbackSlowed = false;
            setTimeout(changeSubtitles, ((target-player.getCurrentTime())/playbackRate)*1000);
        }

        //when the viewer stops in between
        if(secondsTimestamps[secondsTimestamps.indexOf(target)-1] <= currentTimeCeil){
            playbackStopped = true; //on the next 'onStateChange' event this is used to place another timeout
            //console.log("Setting playbackStopped true in changeSubtitles");
            //console.log("Current Time ",player.getCurrentTime()," Target ",target);
        }

        //when the viewer seeks back
        else{
            for (index in secondsTimestamps){
                if (secondsTimestamps[index] > currentTime) {
                    emitChangeSubtitles(index-1);
                    break;
                }
                else if (secondsTimestamps[index] === currentTime) {
                    emitChangeSubtitles(index);
                    break;
                }
            }
        }
    }

    //when the viewer seeks ahead
    else if (Math.floor(currentTime) > target){
        for (index in secondsTimestamps){
            if (secondsTimestamps[index] > Math.floor(currentTime)) {
                emitChangeSubtitles(index-1);
                break;
            }
        }
    }

    //in this case the emitChangeSubtitles will be called
    else if (Math.ceil(currentTime) == target || Math.floor(currentTime) == target){
        var index = secondsTimestamps.indexOf(target);
        emitChangeSubtitles(index);
    }
};
