#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QNetworkAccessManager>

#include "widgets/study/study.h"
#include "widgets/study/study_nptel.h"
#include "widgets/study/study_youtube.h"
#include "widgets/course_searcher/course_searcher.h"
#include "widgets/welcome/welcome.h"
#include "model/model.h"
#include "widgets/revision/revision.h"
#include "widgets/revision/lectureselection/lectureselection.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QString *host, QNetworkAccessManager *netManager, QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void studyTime(QString, QJsonObject*,bool);

    void reloadWelcome();//sent to welcome widget

private slots:
    void studyTimeSlot(QString,QJsonObject*,bool);

    void on_actionNew_Tab_triggered();

    void closeTab(int);

    void on_actionScreenshot_triggered();

    void emitStudyTime(QJsonObject*);//catches studyTime_courseSearcher signal and emits studyTime

    void changeWindowTitle(int);//changes the windows title whenever current tab is changed

    void changeTabTextSlot(QString);//this catches the changeTabText signal from course_searcher and changes the tab text of the current tab
    //this is a bit of problem in the case after searching the user switches to a different tab, but lets see.

    void on_actionMake_Fullscreen_triggered();

    void on_actionReturn_Normal_Size_triggered();

    void reviseTimeSlot(QJsonObject *courseInfo, QJsonObject *currentLectureInfo, QJsonArray *startedLecturesInfo);

    void lectureSelectionSlot(QJsonObject *information);

    void reloadWelcomeSlot();//catches reloadWelcome signal from study widget.

private:
    Ui::MainWindow *ui;

    //in all below int is index of the tab where the widgets where added.
    QMap <int,study *>* studyWidgets;//pointers to studywidgets created by the user
    QMap <int,course_searcher *>* searchWidgets;//pointers to searchwidgets created by the user.
    QMap <int,revision *>* reviseWidgets;//pointers to reviseWidgets created by the user
    QMap <int,lectureSelection *>* lectureSelectionWidgets;//pointers to lectureSelectionWidgets created by the user

    QString *host; //ip of the remote host.
    welcome *welcomeWidget;//for the welcome page
    QNetworkAccessManager *netManager;

    //functions
    void createSearchWidget();//creates a search widget and adds it to the next tab
};

#endif // MAINWINDOW_H
