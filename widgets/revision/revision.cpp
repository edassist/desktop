#include "revision.h"
#include "ui_revision.h"

revision::revision(QWidget *parent, QJsonObject *courseInformation, QJsonObject *currentLectureInfo, QJsonArray *startedLecturesInfo) :
    QWidget(parent),
    ui(new Ui::revision)
{
    ui->setupUi(this);
    this->startedLecturesInfo = *startedLecturesInfo;
    this->info = *courseInformation;
    courseFolder = info.value("course_path").toString();

    //finding the iterator of currentLecturesInfo in startedLecturesInfo
    for(currentLectureIterator = startedLecturesInfo->begin(); currentLectureIterator != startedLecturesInfo->end(); ++currentLectureIterator){
        if(currentLectureInfo->value("lecture_number").toInt() == currentLectureIterator->toObject().value("lecture_number").toInt())
            break;
    }
    ui->backToolButton->setDefaultAction(ui->actiongoBack);
    ui->aheadToolButton->setDefaultAction(ui->actiongoAhead);
    ui->backNotesToolButton->setDefaultAction(ui->actiongoBackNotes);
    ui->aheadNotesToolButton->setDefaultAction(ui->actiongoAheadNotes);

    screenshotScene = new QGraphicsScene(this);
    ui->screenshotGraphicsView->setScene(screenshotScene);

    //load lecture revision stuff
    loadLecture();
}

revision::~revision()
{
    delete ui;
    screenshotScene->deleteLater();
}

void revision::setIndex(int index)
{
    this->index = index;
}

void revision::on_actiongoBack_triggered()
{
    if(sequenceIterator == sequence.begin()) return;//can't go back

    //sequenceNotesIterator points to that notes' file which sequenceIterator points to or has pointed to in past.
    if(sequenceIterator == sequenceNotesIterator-1) --sequenceNotesIterator;
    --sequenceIterator;

    //deciding which function to call
    display();
    return;
}

void revision::on_actiongoAhead_triggered()
{
    if(sequenceIterator == --sequence.end()) {
        //check if more lectures exist
        if(currentLectureIterator != startedLecturesInfo.end()-1){
            QMessageBox::StandardButton response = QMessageBox::question(this,"Revision Completed",
                                                 "Yay you have revised this lecture.\nWant to move on to the next one?",
                                                  QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);
            if(response == QMessageBox::Yes) {
                QJsonObject temp = (++currentLectureIterator)->toObject();
                emit reviseNext(&info,&temp,&startedLecturesInfo);
            }
            else{
                QMessageBox::information(this,"Great Decision","There is no need to rush");
            }
        }
        else return; //if no more lectures exist
    }
    else{
        //sequenceNotesIterator points to that notes' file which sequenceIterator points to or has pointed to in past.
        if(sequenceIterator == sequenceNotesIterator+1) ++sequenceNotesIterator;
        ++sequenceIterator;

        //deciding which function to call
        display();
    }
    return;
}

void revision::on_actiongoBackNotes_triggered()
{
    if(sequenceNotesIterator == sequenceNotes.begin())return;//can't go back
    --sequenceNotesIterator;
    displayNotes(*sequenceNotesIterator);
}

void revision::on_actiongoAheadNotes_triggered()
{
    if(sequenceNotesIterator == sequenceNotes.end()-1) return;//can't go ahead
    ++sequenceNotesIterator;
    displayNotes(*sequenceNotesIterator);
}

void revision::display()
{
    if(sequenceIterator->contains("Screenshot"))displayScreenshot(*sequenceIterator);
    else if(sequenceIterator->contains("Notes"))displayNotes(*sequenceIterator);
    return;
}

void revision::displayScreenshot(QString imageFile)
{
    if(!QFile::exists(QString("%1/%2/%3").arg(courseFolder,lectureFolder,imageFile))){
        QMessageBox::information(this,"ERROR",QString("%1 doesn't exist in %2").arg(imageFile,lectureFolder));
        return;
    }
    QPixmap image(QString("%1/%2/%3").arg(courseFolder,lectureFolder,imageFile));
    screenshotScene->clear();
    screenshotScene->addPixmap(image);
    return;
}

void revision::displayNotes(QString textFile)
{
    QString fileContent;
    fileContent = readFile(QString("%1/%2/%3").arg(courseFolder,lectureFolder,textFile));
    if(fileContent == "ERROR"){
        QMessageBox::information(this,"ERROR",QString("Unable to open %2 in %1. Some Error occurred").arg(lectureFolder,textFile));
        return ;
    }

    ui->notesPlainTextEdit->setPlainText(fileContent);
    return;
}

bool revision::loadSequenceFile()
{
    QString fileContent;
    fileContent = readFile(QString("%1/%2/sequence.txt").arg(courseFolder,lectureFolder));
    if(fileContent == "ERROR") return false;

    QStringList parts = fileContent.split("\n",QString::SkipEmptyParts);
    QStringList :: iterator p;
    for(p=parts.begin(); p!=parts.end(); ++p){
        sequence.append(*p);
        if (p->contains("Notes")) sequenceNotes.append(*p);
    }

    sequenceIterator = sequence.begin();
    sequenceNotesIterator = sequenceNotes.begin();
    return true;
}

void revision::loadLecture()
{
    QJsonObject lectureObject = currentLectureIterator->toObject();
    ui->lectureLabel->setText(lectureObject.value("title").toString());
    lectureFolder = QString("Lec %1 %2").arg(QString::number(lectureObject.value("lecture_number").toInt())).arg(lectureObject.value("title").toString());
    if(!loadSequenceFile()){
        QMessageBox::information(this,"Error",QString("Unable to open sequence.txt in %1. Some Error occurred").arg(lectureFolder));
        return;
    }

    //display whatever is first in sequence
    display();
}

QString revision::readFile(QString filename)
{
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug()<<"ERROR : Unable to open "+file.fileName();
        return "ERROR";
    }

    QString  fileContent;
    QTextStream in(&file);
    QString line = in.readLine();
    while(!line.isNull()){
        fileContent.append(line);
        fileContent.append("\n");//getting the content as it is
        line = in.readLine();
    }
    file.close();

    return fileContent;
}
