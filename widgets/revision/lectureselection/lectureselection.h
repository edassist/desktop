#ifndef LECTURESELECTION_H
#define LECTURESELECTION_H

#include <QWidget>
#include <QJsonArray>

#include "widgets/search_result/search_result.h"

namespace Ui {
class lectureSelection;
}

class lectureSelection : public QWidget
{
    Q_OBJECT

public:
    explicit lectureSelection(QWidget *parent = nullptr, QJsonObject *information = nullptr, QJsonArray *startedLectureInformation = nullptr);
    ~lectureSelection();

public slots:
    void reviseLectureSlot(QJsonObject *information);//catches reviseLecture signal from search_result

signals:
    void reviseTime(QJsonObject *courseInfo, QJsonObject *currentLectureInfo, QJsonArray *startedLectureInfo);

private:
    Ui::lectureSelection *ui;
    int lectureNumber; // this is the lecture the user wants to revise
    QJsonArray startedLectureInfo;//these are the lectures the user has started watching
    QJsonObject info; //course info

    QList <search_result *> * searchResultWidgets;
    search_result* searchResultWidget;
};

#endif // LECTURESELECTION_H
