#include "lectureselection.h"
#include "ui_lectureselection.h"

lectureSelection::lectureSelection(QWidget *parent, QJsonObject *information, QJsonArray *startedLectureInformation) :
    QWidget(parent),
    ui(new Ui::lectureSelection)
{
    ui->setupUi(this);
    info = *information;
    searchResultWidgets = new QList<search_result*>;
    startedLectureInfo = *startedLectureInformation;

    //checking the lecture type
    //first for nptel
    if (info.value("course_provider").toString() == "NPTEL") {
        ui->courseTitleLabel->setText(info.value("subjectname").toString());
    }
    //then for Youtube
    else if(info.value("course_provider").toString() == "YOUTUBE"){

    }

    //create search_result widgets
    QJsonArray :: iterator p;
    QJsonObject temp;
    for (p=startedLectureInfo.begin();p!=startedLectureInfo.end();++p){
        temp = p->toObject();
        searchResultWidget = new search_result(this);
        searchResultWidget->setupLectureSelection(temp.value("lecture_number").toInt(), temp.value("title").toString(), &temp);
        ui->lecturesVerticalLayout->addWidget(searchResultWidget,Qt::AlignTop);

        //some connections
        connect(searchResultWidget,&search_result::reviseLecture,this,&lectureSelection::reviseLectureSlot);
        searchResultWidgets->append(searchResultWidget);
    }
}

lectureSelection::~lectureSelection()
{
    delete ui;

    //deleting all searchResultWidgets;
    QList<search_result *> :: iterator p;
    for (p=searchResultWidgets->begin();p!=searchResultWidgets->end();++p) {
        (*p)->deleteLater();
    }
    searchResultWidgets->clear();
    delete searchResultWidgets;
}

void lectureSelection::reviseLectureSlot(QJsonObject *information)
{
    emit reviseTime(&info, information, &startedLectureInfo);
}

