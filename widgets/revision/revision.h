#ifndef REVISION_H
#define REVISION_H

#include <QWidget>
#include <QJsonObject>
#include <QGraphicsScene>

#include "lectureselection/lectureselection.h"

namespace Ui {
class revision;
}

class revision : public QWidget
{
    Q_OBJECT

public:
    explicit revision(QWidget *parent, QJsonObject *courseInformation, QJsonObject *currentLectureInfo, QJsonArray *startedLecturesInfo);
    ~revision();

    void setIndex(int index);

private slots:
    void on_actiongoBack_triggered();

    void on_actiongoAhead_triggered();

    void on_actiongoBackNotes_triggered();

    void on_actiongoAheadNotes_triggered();

signals:
    //emitted when the user wants to move on to the next lecture (for revision)
    void reviseNext(QJsonObject *courseInformation, QJsonObject *currentLectureInfo, QJsonArray *startedLecturesInfo);

private:
    Ui::revision *ui;
    QJsonObject info;//course info
    QJsonArray startedLecturesInfo;
    QJsonArray :: iterator currentLectureIterator;//points to the current lecture object in startedLecturesInfo array
    QString lectureFolder;// Folder of the current lecture being revised.
    QString courseFolder;//Folder of the course whose lectures are being reviewed
    QStringList sequenceNotes;// holds the sequence in which notes files are to be displayed
    QStringList sequence;// holds the sequence in which notes and screenshots are to be displayed
    QStringList :: iterator sequenceIterator;
    QStringList :: iterator sequenceNotesIterator;
    QGraphicsScene *screenshotScene;
    int index; //index in the tabWidget where this widget is displayed

    //private functions
    void display();//displays whatever the current sequenceIterator points to
    void displayScreenshot(QString imageFile);
    void displayNotes(QString textFile);
    bool loadSequenceFile();//reads the sequence file
    void loadLecture();//loads the lecture to which currentLectureIterator points
    QString readFile(QString filename);
};

#endif // REVISION_H
