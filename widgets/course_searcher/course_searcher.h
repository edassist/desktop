#ifndef COURSE_SEARCHER_H
#define COURSE_SEARCHER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QNetworkAccessManager>
#include <QLabel>
#include <QSplitter>
#include <QStatusBar>
#include <QProgressBar>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QTimer>
#include <QMessageBox>

#include "learn_more/learn_more.h"
#include "learn_more/learn_more_mitocw.h"
#include "learn_more/learn_more_nptel.h"
#include "learn_more/learn_more_udacity.h"
#include "filtersdialog/filtersdialog.h"
#include "widgets/search_result/search_result.h"

namespace Ui {
class course_searcher;
}

class course_searcher : public QWidget //this is the widget used for searching the course catalog
{
    Q_OBJECT

public:
    explicit course_searcher(QString *host, QNetworkAccessManager *netManager, QWidget *parent = nullptr);
    ~course_searcher();

private slots:
    void on_searchBarLineEdit_returnPressed();

    void on_searchPushButton_clicked();

    void networkRequestFinished();

    void networkDataReady();//accepts data arrived and puts in the buffer

    void loading(int ,int);//updates the progress bar while data is being fetched

    void searchRequest(QString );//sends the search request. accepts the search term

    void changeLearnMoreSlot(QJsonObject *info);//this function is called when the changeLearnMore signal is emitted

    void emitStudyTime(QJsonObject*);//accepts studyTime_learnMore signal from learn_more widget and emits studyTime_courseSearcher

    void on_filtersPushButton_clicked();

    void updateFiltersSlot(QJsonObject);

    void loadResults();

    void displayStatusBarMessage(QString, int);

signals:
    void studyTime_courseSearcher(QJsonObject *);//sent to mainwindow

    void changeTabText(QString ); //this is emitted everytime a query is sent to the server.

private:
    Ui::course_searcher *ui;

    QNetworkAccessManager * netManager;
    QNetworkReply * netReply;
    QByteArray * networkDataBuffer;
    QString * host;

    QVBoxLayout *scrollAreaLayout;
    QList <search_result *> * searchResultWidgets;
    learn_more *learnMoreWidget;
    filtersDialog *filterDialog;
    QLabel *noResultLabel;
    QStatusBar *statusBar;
    QProgressBar *searchProgressBar;

    QJsonObject filters;
    QJsonObject searchResults;

    //private functions
    bool filterCheck(QJsonObject *);

    void collapseLearnMoreSlot();

};

#endif // COURSE_SEARCHER_H
