#include "course_searcher.h"
#include "ui_course_searcher.h"


course_searcher::course_searcher(QString *host, QNetworkAccessManager *netManager, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::course_searcher)
{
    ui->setupUi(this);
    this->netManager = netManager;
    netReply=nullptr;
    learnMoreWidget=nullptr;
    networkDataBuffer = new QByteArray();
    searchResultWidgets = new QList <search_result *> ;
    scrollAreaLayout = new QVBoxLayout;
    filters={}; //empty filters
    noResultLabel = nullptr;
    filterDialog = new filtersDialog;
    this->host = host;
    statusBar= new QStatusBar(this);
    searchProgressBar = new QProgressBar(this);

    //part of making scrollArea only scroll in the vertical direction
    ui->scrollArea->setWidgetResizable(true);    
    ui->searchResultVerticalLayout->addLayout(scrollAreaLayout);

    ui->statusBarHorizontalLayout->addWidget(statusBar);
    statusBar->setSizeGripEnabled(false);
    statusBar->hide();
    statusBar->addPermanentWidget(searchProgressBar);
    searchProgressBar->hide();

    //connections
    connect(filterDialog,&filtersDialog::updateFilters,this,&course_searcher::updateFiltersSlot);
}

course_searcher::~course_searcher()
{
    delete networkDataBuffer;
    scrollAreaLayout->deleteLater();
    learnMoreWidget->deleteLater();
    filterDialog->deleteLater();
    if (noResultLabel != nullptr) noResultLabel->deleteLater();
    statusBar->deleteLater();
    searchProgressBar->deleteLater();

    //deleting all searchResultWidgets;
    QList<search_result *> :: iterator p;
    for (p=searchResultWidgets->begin();p!=searchResultWidgets->end();++p) {
        (*p)->deleteLater();
    }
    searchResultWidgets->clear();
    delete searchResultWidgets;

    delete ui;
}

void course_searcher::changeLearnMoreSlot(QJsonObject *info)
{
    if (learnMoreWidget != nullptr){
        learnMoreWidget->deleteLater();
        learnMoreWidget = nullptr;
    }

    if (info->value("course_provider") == "NPTEL") learnMoreWidget=new learn_more_nptel(this,info);
    else if (info->value("course_provider") == "UDACITY") learnMoreWidget = new learn_more_udacity(this,info);
    else if (info->value("course_provider") == "MITOCW") learnMoreWidget = new learn_more_mitocw(this,info);

    ui->splitter->addWidget(learnMoreWidget);

    connect(learnMoreWidget,&learn_more::studyTime_learnMore,this,&course_searcher::emitStudyTime);
    connect(learnMoreWidget,&learn_more::collapseLearnMore,this,&course_searcher::collapseLearnMoreSlot);
    connect(learnMoreWidget,&learn_more::displayError,this,&course_searcher::displayStatusBarMessage);
}

void course_searcher::emitStudyTime(QJsonObject * information)
{
    emit studyTime_courseSearcher(information);
}

void course_searcher::on_searchBarLineEdit_returnPressed()
{
    //checking the query string
    QString query = ui->searchBarLineEdit->text();
    if(query == "nptel"){// because /search/nptel is an api endpoint
        QMessageBox::information(this,"ERROR","Query string not allowed. Search something else");
        return;
    }
    searchRequest(query);
    emit changeTabText("Search : "+ui->searchBarLineEdit->text());
    return;
}

void course_searcher::on_searchPushButton_clicked()
{
    //checking the query string
    QString query = ui->searchBarLineEdit->text();
    if(query == "nptel"){ // because /search/nptel is an api endpoint
        QMessageBox::information(this,"ERROR","Query string not allowed. Search something else");
        return;
    }
    searchRequest(query);
    emit changeTabText("Search : "+ui->searchBarLineEdit->text());
    return;
}

void course_searcher::networkRequestFinished()
{
    searchProgressBar->hide();
    if (netReply->error() != QNetworkReply::NoError){
        if(netReply->error() == QNetworkReply::ConnectionRefusedError){\
            displayStatusBarMessage("Remote host refused connection. Try again later",5000);
        }
        else displayStatusBarMessage("Some error occurred",5000);
        netReply->abort();
    }
    else {
        QJsonDocument searchResults= QJsonDocument::fromJson(*networkDataBuffer);
        this->searchResults=searchResults.object();
        loadResults();
    }

    networkDataBuffer->clear();
    netReply->deleteLater();
}

void course_searcher::networkDataReady()
{
    networkDataBuffer->append(netReply->readAll());
}

void course_searcher::loading(int bytesDownloaded, int bytesTotal)
{
    if(bytesTotal == 0) return;
    searchProgressBar->setValue( (bytesDownloaded/bytesTotal)*100 );
    return;
}

void course_searcher::searchRequest(QString query)
{
    QNetworkRequest request;
    request.setHeader(QNetworkRequest::UserAgentHeader,QVariant(QString("EdAssist-Qt")));
    request.setUrl(QUrl(*host+"/search/"+query));
    netReply=netManager->get(request);

    searchProgressBar->show();
    connect(netReply,&QNetworkReply::finished,this,&course_searcher::networkRequestFinished);
    connect(netReply,&QIODevice::readyRead,this,&course_searcher::networkDataReady);
    connect(netReply,&QNetworkReply::downloadProgress,this,&course_searcher::loading);

    return ;
}

void course_searcher::on_filtersPushButton_clicked()
{
    filterDialog->exec();
    return;
}

void course_searcher::updateFiltersSlot(QJsonObject filtersSent)
{
    this->filters = filtersSent;
    loadResults();
}

void course_searcher::loadResults()
{
    //the case where previously there were no results
    if(noResultLabel != nullptr){
        noResultLabel->deleteLater();
        noResultLabel=nullptr;
    }

    //destroying all previous searchResult widgets
    for (int i = 0;i<searchResultWidgets->size();++i){
        scrollAreaLayout->removeWidget(searchResultWidgets->value(i));
        searchResultWidgets->value(i)->deleteLater();
    }
    searchResultWidgets->clear();

    //destroying learnMoreWidget if it exists
    if(! (learnMoreWidget == nullptr)){
        learnMoreWidget->deleteLater();
        learnMoreWidget=nullptr;
    }

    //to understand this see how the results re obtained from the server
    QJsonObject object ,jsonObject=searchResults;
    QJsonArray jsonArray ;
    QStringList keys = jsonObject.keys();

    QStringList ::iterator q;
    for (q=keys.begin();q!=keys.end();++q) {
        jsonArray=jsonObject[*q].toArray();
        QJsonArray::iterator p;

        for (p=jsonArray.begin();p!=jsonArray.end();++p) {
            object=p->toObject();
            object.insert("course_provider",QJsonValue(q->toUpper()));
            if (!filterCheck(&object)) continue;

            search_result *searchResult=new search_result(this);
            searchResult->populateInfo(object);
            searchResultWidgets->append(searchResult);
            scrollAreaLayout->addWidget(searchResult);

            connect(searchResult,&search_result::changeLearnMore,this,&course_searcher::changeLearnMoreSlot);
        }
    }

    if (searchResultWidgets->empty()){
        noResultLabel = new QLabel("No Result Found. Please try changing the filters or search query");
        scrollAreaLayout->addWidget(noResultLabel);
    }
}

void course_searcher::displayStatusBarMessage(QString message, int timeout)
{
    statusBar->show();
    statusBar->showMessage(message,timeout);
    QTimer::singleShot(timeout,statusBar,SLOT(hide()));
}

bool course_searcher::filterCheck(QJsonObject *object)
{
    if (filters.empty())return true;

    if(filters.contains(object->value("course_provider").toString())){
        QJsonObject temp = filters.value(object->value("course_provider").toString()).toObject();

        if (temp.empty()) return true; //in the case when user ,say ,only presses NPTEL but nothing from more options.

        QStringList :: iterator p;
        QJsonArray :: iterator q;
        QStringList keys = temp.keys();

        if (object->value("course_provider").toString() == "MITOCW"){
            for (p=keys.begin();p!=keys.end();++p) {
                if ((*p)=="department" || (*p)=="level"){
                    //first we deal with array types
                    QJsonArray array = temp.value(*p).toArray();
                    QString target = object->value(*p).toString();//the array needs to contain this to be accepted by the filter.
                    bool chk = false;
                    for (q=array.begin();q!=array.end();++q) {
                        if((*q).toString() == target){
                            chk=true;
                            break;
                        }
                    }
                    if(!chk) return false;
                }
                else {
                    //now we deal with boolean types
                    bool target2=object->value(*p).toBool();
                    if (target2 != temp.value(*p).toBool()) return false;
                }
            }
            return true;
        }
        else if (object->value("course_provider").toString() == "NPTEL"){
            for (p=keys.begin();p!=keys.end();++p) { //nptel has only array types
                QJsonArray array = temp.value(*p).toArray();
                QString target = object->value(*p).toString();//the array needs to contain this to be accepted by the filter.
                bool chk = false;
                for (q=array.begin();q!=array.end();++q) {
                    if((*q).toString() == target){
                        chk=true;
                        break;
                    }
                }
                if(!chk) return false;
            }
            return true;
        }
        else if(object->value("course_provider").toString() == "UDACITY") {
            for (p=keys.begin();p!=keys.end();++p) {
                if ((*p)=="school"){
                    //first we deal with
                    QJsonArray array = temp.value(*p).toArray();
                    QString target = object->value(*p).toString();//the array needs to contain this to be accepted by the filter.
                    bool chk = false;
                    for (q=array.begin();q!=array.end();++q) {
                        if((*q).toString() == target){
                            chk=true;
                            break;
                        }
                    }
                    if(!chk) return false;
                }
                else if((*p)=="difficulty"){
                    //first we deal with
                    QJsonArray array = temp.value(*p).toArray();
                    int target3 = object->value(*p).toInt();//the array needs to contain this to be accepted by the filter.
                    bool chk = false;
                    for (q=array.begin();q!=array.end();++q) {
                        if((*q).toInt() == target3){
                            chk=true;
                            break;
                        }
                    }
                    if(!chk) return false;
                }
                else {//deals with the fact 'paid' may or may not exist in temp
                    //now we deal with boolean types
                    bool target2=object->value(*p).toBool();
                    if (target2 != temp.value(*p).toBool()) return false;
                }
            }
            return true;
        }
    }
    else return false;
}

void course_searcher::collapseLearnMoreSlot()
{
    learnMoreWidget->deleteLater();
    learnMoreWidget=nullptr;
    return;
}
