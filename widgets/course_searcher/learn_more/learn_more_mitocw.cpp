#include "learn_more_mitocw.h"
#include "ui_learn_more_mitocw.h"

learn_more_mitocw::learn_more_mitocw(QWidget *parent, QJsonObject *information) :
    learn_more(parent),
    ui(new Ui::learn_more_mitocw)
{
    ui->setupUi(this);

    info=information;
    ui->departmentLabel->setText(info->value("department").toString());
    ui->semesterLabel->setText(info->value("semester").toString());
    ui->levelLabel->setText(info->value("level").toString());
    ui->courseNumberLabel->setText(info->value("course_number").toString());
    ui->descriptionPlainTextEdit->setPlainText(info->value("description").toString());
    ui->featuresPlainTextEdit->setPlainText(info->value("features").toString());
    ui->instructorsLabel->setText(info->value("instructors").toString());
    titleLabel->setText(info->value("title").toString());

    ui->toolBarHorizontalLayout->addWidget(toolbar);
}

learn_more_mitocw::~learn_more_mitocw()
{
    delete ui;
}

QJsonObject *learn_more_mitocw::getInfo()
{
    return info;
}

QWidget *learn_more_mitocw::getWidgetPointer()
{
    return this;
}

void learn_more_mitocw::on_viewCoursePushButton_clicked()
{
    if (!QDesktopServices::openUrl(QUrl(info->value("url").toString()))){
        emit displayError("Could not open"+info->value("url").toString()+"in your web browser",5000);
    }
}
