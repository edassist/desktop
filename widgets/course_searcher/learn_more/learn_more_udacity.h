#ifndef LEARN_MORE_UDACITY_H
#define LEARN_MORE_UDACITY_H

#include "learn_more.h"
#include <QJsonObject>
#include <QDesktopServices>
#include <QUrl>
#include <QWidget>

namespace Ui {
class learn_more_udacity;
}

class learn_more_udacity : public learn_more
{
    //handles display of udacity content when learn more button is pushed in course searcher
    Q_OBJECT

public:
    explicit learn_more_udacity(QWidget *parent = nullptr,QJsonObject *info =nullptr);
    ~learn_more_udacity();

    QJsonObject* getInfo();
    QWidget* getWidgetPointer();

private slots:
    void on_viewCoursePushButton_clicked();

private:
    Ui::learn_more_udacity *ui;

    QJsonObject *info; //this contains the individual search result information
};

#endif // LEARN_MORE_UDACITY_H
