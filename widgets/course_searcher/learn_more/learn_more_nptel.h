#ifndef LEARN_MORE_NPTEL_H
#define LEARN_MORE_NPTEL_H

#include <QWidget>
#include "learn_more.h"
#include <QJsonObject>

namespace Ui {
class learn_more_nptel;
}

class learn_more_nptel : public learn_more
{
    //handles display of nptel content when learn more button is pushed in course searcher
    Q_OBJECT

public:
    explicit learn_more_nptel(QWidget *parent = nullptr,QJsonObject *info =nullptr);
    ~learn_more_nptel();

    QJsonObject* getInfo();
    QWidget* getWidgetPointer();

private slots:
    void on_startCoursePushButton_clicked();

private:
    Ui::learn_more_nptel *ui;

    QJsonObject *info; //this contains the individual search result information
};

#endif // LEARN_MORE_NPTEL_H
