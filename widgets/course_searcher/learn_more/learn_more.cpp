#include "learn_more.h"
#include <QDebug>

learn_more::learn_more(QWidget *parent) :
    QWidget(parent)
{
    toolbar=new QToolBar(this);
    collapseButton = new QToolButton(this);
    titleLabel = new QLabel(this);
    toolbar->addWidget(collapseButton);
    toolbar->addWidget(titleLabel);
    closeIcon = new QIcon(":/images/rightArrow.jpeg");
    collapseButton->setIcon(*closeIcon);

    connect(collapseButton,&QToolButton::clicked,this,&learn_more::collapseButtonClicked);
}

learn_more::~learn_more()
{
    toolbar->deleteLater();
    titleLabel->deleteLater();
    collapseButton->deleteLater();
    delete closeIcon;

    return;
}

void learn_more::collapseButtonClicked()
{
    emit collapseLearnMore();
}


