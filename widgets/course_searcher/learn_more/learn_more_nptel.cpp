#include "learn_more_nptel.h"
#include "ui_learn_more_nptel.h"

learn_more_nptel::learn_more_nptel(QWidget *parent, QJsonObject *information) :
    learn_more(parent),
    ui(new Ui::learn_more_nptel)
{
    ui->setupUi(this);

    info = information;
    //some stuff initialzed in parent class constructor
    ui->toolBarHorizontalLayout->addWidget(toolbar);

    titleLabel->setText(info->value("subjectname").toString());
    ui->disciplineLabel->setText(info->value("disciplinename").toString());
    ui->instituteLabel->setText(info->value("institute").toString());
    ui->mediumLabel->setText(info->value("medium").toString());
    ui->coordinatorsLabel->setText(info->value("coordinators").toString());

}

learn_more_nptel::~learn_more_nptel()
{
    delete ui;
}

QJsonObject *learn_more_nptel::getInfo()
{
    return info;
}

QWidget *learn_more_nptel::getWidgetPointer()
{
    return this;
}

void learn_more_nptel::on_startCoursePushButton_clicked()
{
    emit studyTime_learnMore(info);
}
