#ifndef LEARN_MORE_H
#define LEARN_MORE_H

#include <QWidget>
#include <QJsonObject>
#include <QToolBar>
#include <QLabel>
#include <QToolButton>

class learn_more : public QWidget
{
    //this is the parent class of types of learn_more stuff.
    //this is used so that in course_searcher only 1 learn_more list is to be declared of the type learn_more
    //virtual functions will be used to obtain data from derived classes when needed
    Q_OBJECT

public:
    explicit learn_more(QWidget *parent = nullptr);
    ~learn_more();

    virtual QJsonObject * getInfo() =0;
    virtual QWidget* getWidgetPointer() =0;

signals:
    void studyTime_learnMore(QJsonObject *);//studyTime signal from learn_more widget.Sent to course_searcher and from there to mainWindow
    void collapseLearnMore();
    void displayError(QString ,int);//caught by displayStatusBarMessage, to display error messages if they happen.

protected:
    QToolBar *toolbar;
    QLabel *titleLabel;
    QToolButton *collapseButton;
    QIcon *closeIcon;

protected slots:
    void collapseButtonClicked();
};

#endif // LEARN_MORE_H
