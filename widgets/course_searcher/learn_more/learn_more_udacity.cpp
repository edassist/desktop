#include "learn_more_udacity.h"
#include "ui_learn_more_udacity.h"

learn_more_udacity::learn_more_udacity(QWidget *parent, QJsonObject *information) :
    learn_more(parent),
    ui(new Ui::learn_more_udacity)
{
    ui->setupUi(this);

    info=information;
    titleLabel->setText(info->value("title").toString());
    ui->schoolLabel->setText(info->value("school").toString());
    ui->toolbarHorizontalLayout->addWidget(toolbar);

    if (info->value("paid").toBool()) ui->feesLabel->setText("Paid");
    else ui->feesLabel->setText("Free");
    if(info->value("difficulty").toInt() == 1)ui->difficultyLabel->setText("Beginner");
    if(info->value("difficulty").toInt() == 2)ui->difficultyLabel->setText("Intermediate");
    if(info->value("difficulty").toInt() == 3)ui->difficultyLabel->setText("Advanced");
}

learn_more_udacity::~learn_more_udacity()
{
    delete ui;
}

QJsonObject* learn_more_udacity::getInfo()
{
    return info;
}

QWidget *learn_more_udacity::getWidgetPointer()
{
    return this;
}

void learn_more_udacity::on_viewCoursePushButton_clicked()
{
    if (!QDesktopServices::openUrl(QUrl(info->value("url").toString()))){
        emit displayError("Could not open"+info->value("url").toString()+"in your web browser",5000);
    }
    return;
}
