#include "filtersdialog_nptel.h"
#include "ui_filtersdialog_nptel.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

filtersdialog_nptel::filtersdialog_nptel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::filtersdialog_nptel)
{
    ui->setupUi(this);

    filters={};
}

filtersdialog_nptel::~filtersdialog_nptel()
{
    delete ui;
}

QJsonObject filtersdialog_nptel::getdata()
{
    filters={};//initializing to empty. reason mentioned in filtersdialog.cpp
    QJsonArray instituteArray, disciplineArray, contentArray;
    QStringList :: iterator p;
    for (p=institutes.begin();p!=institutes.end();++p) instituteArray.append(QJsonValue(*p));
    for (p=disciplineName.begin();p!=disciplineName.end();++p) disciplineArray.append(QJsonValue(*p));
    for (p=contentType.begin();p!=contentType.end();++p) contentArray.append(QJsonValue(*p));

    if(!instituteArray.empty()) filters.insert("institute",instituteArray);
    if(!disciplineArray.empty())filters.insert("disciplinename",disciplineArray);
    if(!contentArray.empty())filters.insert("medium",contentArray);

    return filters;
}

void filtersdialog_nptel::on_pushButton_clicked()
{
    //unchecking everything

    //first disciplinename
    ui->agricultureCheckBox->setCheckState(Qt::Unchecked);
    ui->aerospaceEngCheckBox->setCheckState(Qt::Unchecked);
    ui->architectureCheckBox->setCheckState(Qt::Unchecked);
    ui->atmosphericScienceCheckBox->setCheckState(Qt::Unchecked);
    ui->biotechCheckBox->setCheckState(Qt::Unchecked);
    ui->basicCoursesCheckBox->setCheckState(Qt::Unchecked);
    ui->csEngCheckBox->setCheckState(Qt::Unchecked);
    ui->civilEngCheckBox->setCheckState(Qt::Unchecked);
    ui->chemicalEngCheckBox->setCheckState(Qt::Unchecked);
    ui->chemistryBiochemistryCheckBox->setCheckState(Qt::Unchecked);
    ui->eeEngCheckBox->setCheckState(Qt::Unchecked);
    ui->eceEngCheckBox->setCheckState(Qt::Unchecked);
    ui->engDesignCheckBox->setCheckState(Qt::Unchecked);
    ui->engDesignCheckBox->setCheckState(Qt::Unchecked);
    ui->enviromentCheckBox->setCheckState(Qt::Unchecked);
    ui->generalCheckBox->setCheckState(Qt::Unchecked);
    ui->humanitiesCheckBox->setCheckState(Qt::Unchecked);
    ui->mathsCheckBox->setCheckState(Qt::Unchecked);
    ui->mechEngCheckBox->setCheckState(Qt::Unchecked);
    ui->miningEngCheckBox->setCheckState(Qt::Unchecked);
    ui->managementCheckBox->setCheckState(Qt::Unchecked);
    ui->metallurgyCheckBox->setCheckState(Qt::Unchecked);
    ui->multipdisciplinaryCheckBox->setCheckState(Qt::Unchecked);
    ui->nanotechCheckBox->setCheckState(Qt::Unchecked);
    ui->oceanEngCheckBox->setCheckState(Qt::Unchecked);
    ui->physicsCheckBox->setCheckState(Qt::Unchecked);
    ui->specialSeriesCheckBox->setCheckState(Qt::Unchecked);
    ui->textileEngheCckBox->setCheckState(Qt::Unchecked);

    //now institutes
    ui->iiscCheckBox->setCheckState(Qt::Unchecked);
    ui->iitBCheckBox->setCheckState(Qt::Unchecked);
    ui->iitDCheckBox->setCheckState(Qt::Unchecked);
    ui->iitGcheckBox->setCheckState(Qt::Unchecked);
    ui->iitMCheckBox->setCheckState(Qt::Unchecked);
    ui->iitRCheckBox->setCheckState(Qt::Unchecked);
    ui->iitKanpurCheckBox->setCheckState(Qt::Unchecked);
    ui->iitKharagpurCheckBox->setCheckState(Qt::Unchecked);

    //content type
    ui->videoCheckBox->setCheckState(Qt::Unchecked);
    ui->webCheckBox->setCheckState(Qt::Unchecked);

    return;
}

void filtersdialog_nptel::on_aerospaceEngCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Aerospace Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Aerospace Engineering");
    return;
}

void filtersdialog_nptel::on_agricultureCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Agriculture");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Agriculture");
    return;
}

void filtersdialog_nptel::on_architectureCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Architecture");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Architecture");
    return;
}

void filtersdialog_nptel::on_atmosphericScienceCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Atmospheric Science");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Atmospheric Science");
    return;
}

void filtersdialog_nptel::on_basicCoursesCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Basic courses(Sem 1 and 2)");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Basic courses(Sem 1 and 2)");
    return;
}

void filtersdialog_nptel::on_biotechCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Biotechnology");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Biotechnology");
    return;
}

void filtersdialog_nptel::on_chemicalEngCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Chemical Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Chemical Engineering");
    return;
}

void filtersdialog_nptel::on_chemistryBiochemistryCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Chemistry and Biochemistry");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Chemistry and Biochemistry");
    return;
}

void filtersdialog_nptel::on_civilEngCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Civil Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Civil Engineering");
    return;
}

void filtersdialog_nptel::on_csEngCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Computer Science and  Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Computer Science and  Engineering");
    return;
}

void filtersdialog_nptel::on_eeEngCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Electrical Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Electrical Engineering");
    return;
}

void filtersdialog_nptel::on_eceEngCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Electronics & Communication Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Electronics & Communication Engineering");
    return;
}

void filtersdialog_nptel::on_engDesignCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Engineering Design");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Engineering Design");
    return;
}

void filtersdialog_nptel::on_enviromentCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Environmental Science");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Environmental Science");
    return;
}

void filtersdialog_nptel::on_generalCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("General");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("General");
    return;
}

void filtersdialog_nptel::on_humanitiesCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Humanities and Social Sciences");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Humanities and Social Sciences");
    return;
}

void filtersdialog_nptel::on_managementCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Management");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Management");
    return;
}

void filtersdialog_nptel::on_mathsCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Mathematics");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Mathematics");
    return;
}

void filtersdialog_nptel::on_mechEngCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Mechanical Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Mechanical Engineering");
    return;
}

void filtersdialog_nptel::on_metallurgyCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Metallurgy and Material Science");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Metallurgy and Material Science");
    return;
}

void filtersdialog_nptel::on_miningEngCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Mining Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Mining Engineering");
    return;
}

void filtersdialog_nptel::on_multipdisciplinaryCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Multidisciplinary");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Multidisciplinary");
    return;
}

void filtersdialog_nptel::on_nanotechCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Nanotechnology");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Nanotechnology");
    return;
}

void filtersdialog_nptel::on_oceanEngCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Ocean Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Ocean Engineering");
    return;
}

void filtersdialog_nptel::on_physicsCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Physics");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Physics");
    return;
}

void filtersdialog_nptel::on_specialSeriesCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Special Series");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Special Series");
    return;
}

void filtersdialog_nptel::on_textileEngheCckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) disciplineName.append("Textile Engineering");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Textile Engineering");
    return;
}

void filtersdialog_nptel::on_videoCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("Video");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Video");
    return;
}

void filtersdialog_nptel::on_webCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("Web");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("Web");
    return;
}

void filtersdialog_nptel::on_iiscCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("IISc Bangalore");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("IISc Bangalore");
    return;
}

void filtersdialog_nptel::on_iitBCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("IIT Bombay");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("IIT Bombay");
    return;
}

void filtersdialog_nptel::on_iitDCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("IIT Delhi");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("IIT Delhi");
    return;
}

void filtersdialog_nptel::on_iitGcheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("IIT Guwahati");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("IIT Guwahati");
    return;
}

void filtersdialog_nptel::on_iitKanpurCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("IIT Kanpur");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("IIT Kanpur");
    return;
}

void filtersdialog_nptel::on_iitKharagpurCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("IIT Kharagpur");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("IIT Kharagpur");
    return;
}

void filtersdialog_nptel::on_iitMCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("IIT Madras");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("IIT Madras");
    return;
}

void filtersdialog_nptel::on_iitRCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) contentType.append("IIT Roorkee");
    if (arg1 == Qt::Unchecked)disciplineName.removeOne("IIT Roorkee");
    return;
}
