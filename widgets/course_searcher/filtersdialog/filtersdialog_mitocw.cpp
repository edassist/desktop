#include "filtersdialog_mitocw.h"
#include "ui_filtersdialog_mitocw.h"
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

filtersdialog_mitocw::filtersdialog_mitocw(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::filtersdialog_mitocw)
{
    ui->setupUi(this);
    filters={};
}

filtersdialog_mitocw::~filtersdialog_mitocw()
{
    delete ui;
}

QJsonObject filtersdialog_mitocw::getData()
{
    //filtersDialog is only created once by course_searcher so these 2 lines.
    if(filters.contains("department")) filters.remove("department");
    if(filters.contains("level")) filters.remove("level");

    QJsonArray departmentArray, levelArray;
    QStringList ::iterator p;

    for (p=departments.begin();p!=departments.end();++p) departmentArray.append(QJsonValue(*p));
    for (p=levels.begin();p!=levels.end();++p) levelArray.append(QJsonValue(*p));

    if(!departmentArray.isEmpty()) filters.insert("department",departmentArray);
    if(!levelArray.isEmpty())filters.insert("level",levelArray);

    return filters;
}

void filtersdialog_mitocw::on_pushButton_clicked()
{
    //unchecking everything

    //first departments
    ui->artsCheckBox->setCheckState(Qt::Unchecked);
    ui->aeronauticsCheckBox->setCheckState(Qt::Unchecked);
    ui->anthropologyCheckBox->setCheckState(Qt::Unchecked);
    ui->architectureCheckBox->setCheckState(Qt::Unchecked);
    ui->biologyCheckBox->setCheckState(Qt::Unchecked);
    ui->biologicalEngCheckBox->setCheckState(Qt::Unchecked);
    ui->chemistryCheckBox->setCheckState(Qt::Unchecked);
    ui->concourseCheckBox->setCheckState(Qt::Unchecked);
    ui->chemicalEngCheckBox->setCheckState(Qt::Unchecked);
    ui->comparitiveMediaCheckBox->setCheckState(Qt::Unchecked);
    ui->cognitiveSciencesCheckBox->setCheckState(Qt::Unchecked);
    ui->civilEnvironmentalEngCheckBox->setCheckState(Qt::Unchecked);
    ui->dataSystemsCheckBox->setCheckState(Qt::Unchecked);
    ui->economicsCheckBox->setCheckState(Qt::Unchecked);
    ui->edgertonCenterCheckBox->setCheckState(Qt::Unchecked);
    ui->electricalCSEngCheckBox->setCheckState(Qt::Unchecked);
    ui->engineeringSystemsCheckBox->setCheckState(Qt::Unchecked);
    ui->experimentalStudyGroupCheckBox->setCheckState(Qt::Unchecked);
    ui->globalStudiesCheckBox->setCheckState(Qt::Unchecked);
    ui->historyCheckBox->setCheckState(Qt::Unchecked);
    ui->healthSciencesCheckBox->setCheckState(Qt::Unchecked);
    ui->literatureCheckBox->setCheckState(Qt::Unchecked);
    ui->mathsCheckBox->setCheckState(Qt::Unchecked);
    ui->mechEngCheckBox->setCheckState(Qt::Unchecked);
    ui->mediaArtsCheckBox->setCheckState(Qt::Unchecked);
    ui->managementCheckBox->setCheckState(Qt::Unchecked);
    ui->materialScienceEngCheckBox->setCheckState(Qt::Unchecked);
    ui->nuclearEngCheckBox->setCheckState(Qt::Unchecked);
    ui->physicsCheckBox->setCheckState(Qt::Unchecked);
    ui->philosphyCheckBox->setCheckState(Qt::Unchecked);
    ui->politicalScienceCheckBox->setCheckState(Qt::Unchecked);
    ui->planetarySciencesCheckBox->setCheckState(Qt::Unchecked);
    ui->PECheckBox->setCheckState(Qt::Unchecked);
    ui->sciTechSocietyCheckBox->setCheckState(Qt::Unchecked);
    ui->supplementalResourcesCheckBox->setCheckState(Qt::Unchecked);
    ui->urbanStudiesCheckBox->setCheckState(Qt::Unchecked);
    ui->womenGenderStudiesCheckBox->setCheckState(Qt::Unchecked);


    //now features
    ui->assessmentSolutionsCheckBox->setCheckState(Qt::Unchecked);
    ui->completeAudioCheckBox->setCheckState(Qt::Unchecked);
    ui->completeVideoCheckBox->setCheckState(Qt::Unchecked);
    ui->completeLecturesCheckBox->setCheckState(Qt::Unchecked);
    ui->instructorInsightsCheckBox->setCheckState(Qt::Unchecked);
    ui->studentProjectsCheckBox->setCheckState(Qt::Unchecked);
    ui->otherAudioCheckBox->setCheckState(Qt::Unchecked);
    ui->otherVideoCheckBox->setCheckState(Qt::Unchecked);
    ui->onlineTextbooksCheckBox->setCheckState(Qt::Unchecked);

    //levels
    ui->graduateCheckBox->setCheckState(Qt::Unchecked);
    ui->undergraduateCheckBox->setCheckState(Qt::Unchecked);
}

void filtersdialog_mitocw::on_aeronauticsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Aeronautics and Astronautics");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Aeronautics and Astronautics");
    return;
}

void filtersdialog_mitocw::on_completeVideoCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked) {
        completeVideo=true;
        filters.insert("complete_video",true);
    }
    else if(arg1 == Qt::Unchecked) {
        completeVideo=false;
        filters.remove("complete_video");
    }
    return;
}

void filtersdialog_mitocw::on_completeAudioCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked) {
        completeAudio=true;
        filters.insert("complete_audio",true);
    }
    else if(arg1 == Qt::Unchecked){
        completeAudio=false;
        filters.remove("complete_audio");
    }
    return;
}

void filtersdialog_mitocw::on_otherVideoCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked) {
        otherVideo=true;
        filters.insert("other_video",true);
    }
    else if(arg1 == Qt::Unchecked){
        otherVideo=false;
        filters.remove("other_video");
    }
    return;
}

void filtersdialog_mitocw::on_otherAudioCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked){
        otherAudio=true;
        filters.insert("other_audio",true);
    }
    else if(arg1 == Qt::Unchecked){
        otherAudio=false;
        filters.remove("other_audio");
    }
    return;
}

void filtersdialog_mitocw::on_onlineTextbooksCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked){
        onlineTextbooks=true;
        filters.insert("online_textbooks",true);
    }
    else if(arg1 == Qt::Unchecked){
        onlineTextbooks=false;
        filters.remove("online_textbooks");
    }
    return;
}

void filtersdialog_mitocw::on_completeLecturesCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked){
        completeLectures=true;
        filters.insert("complete_lectures",true);
    }
    else if(arg1 == Qt::Unchecked){
        completeLectures=false;
        filters.remove("complete_lectures");
    }
    return;
}

void filtersdialog_mitocw::on_assessmentSolutionsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked){
        assessmentsSolutions=true;
        filters.insert("assesments_with_solution",true);
    }
    else if(arg1 == Qt::Unchecked){
        assessmentsSolutions=false;
        filters.remove("assesments_with_solution");
    }
    return;
}

void filtersdialog_mitocw::on_studentProjectsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked){
        studentProjects=true;
        filters.insert("student_projects",true);
    }
    else if(arg1 == Qt::Unchecked){
        studentProjects=false;
        filters.remove("student_projects");
    }
    return;
}

void filtersdialog_mitocw::on_instructorInsightsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked){
        instructorInsights=true;
        filters.insert("instructor_insights",true);
    }
    else if(arg1 == Qt::Unchecked){
        instructorInsights=false;
        filters.remove("instructor_insights");
    }
    return;
}

void filtersdialog_mitocw::on_undergraduateCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked) levels.append("Undergraduate");
    else if(arg1 == Qt::Unchecked) levels.removeOne("Undergraduate");
    return;
}

void filtersdialog_mitocw::on_graduateCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked) levels.append("Graduate");
    else if(arg1 == Qt::Unchecked) levels.removeOne("Graduate");
    return;
}

void filtersdialog_mitocw::on_anthropologyCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Anthropology");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Anthropology");
    return;
}

void filtersdialog_mitocw::on_architectureCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Architecture");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Architecture");
    return;
}

void filtersdialog_mitocw::on_PECheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Athletics, Physical Education and Recreation");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Athletics, Physical Education and Recreation");
    return;
}

void filtersdialog_mitocw::on_biologicalEngCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Biological Engineering");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Biological Engineering");
    return;
}

void filtersdialog_mitocw::on_biologyCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Biology");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Biology");
    return;

}

void filtersdialog_mitocw::on_cognitiveSciencesCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Brain and Cognitive Sciences");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Brain and Cognitive Sciences");
    return;
}

void filtersdialog_mitocw::on_chemicalEngCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Chemical Engineering");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Chemical Engineering");
    return;
}

void filtersdialog_mitocw::on_chemistryCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Chemistry");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Chemistry");
    return;
}

void filtersdialog_mitocw::on_civilEnvironmentalEngCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Civil and Environmental Engineering");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Civil and Environmental Engineering");
    return;
}

void filtersdialog_mitocw::on_comparitiveMediaCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Comparative Media Studies/Writing");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Comparative Media Studies/Writing");
    return;
}

void filtersdialog_mitocw::on_concourseCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Concourse");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Concourse");
    return;
}

void filtersdialog_mitocw::on_planetarySciencesCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Earth, Atmospheric, and Planetary Sciences");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Earth, Atmospheric, and Planetary Sciences");
    return;
}

void filtersdialog_mitocw::on_economicsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Economics");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Economics");
    return;
}

void filtersdialog_mitocw::on_edgertonCenterCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Edgerton Center");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Edgerton Center");
    return;
}

void filtersdialog_mitocw::on_electricalCSEngCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Electrical Engineering and Computer Science");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Electrical Engineering and Computer Science");
    return;
}

void filtersdialog_mitocw::on_engineeringSystemsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Engineering Systems Division");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Engineering Systems Division");
    return;
}

void filtersdialog_mitocw::on_experimentalStudyGroupCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Experimental Study Group");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Experimental Study Group");
    return;
}

void filtersdialog_mitocw::on_globalStudiesCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Global Studies and Languages");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Global Studies and Languages");
    return;
}

void filtersdialog_mitocw::on_healthSciencesCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Health Sciences and Technology");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Health Sciences and Technology");
    return;
}

void filtersdialog_mitocw::on_historyCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("History");
    else if(arg1 == Qt::Unchecked)departments.removeOne("History");
    return;
}

void filtersdialog_mitocw::on_dataSystemsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Institute for Data, Systems, and Society");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Institute for Data, Systems, and Society");
    return;
}

void filtersdialog_mitocw::on_philosphyCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Linguistics and Philosophy");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Linguistics and Philosophy");
    return;
}

void filtersdialog_mitocw::on_literatureCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Literature");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Literature");
    return;
}

void filtersdialog_mitocw::on_materialScienceEngCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Materials Science and Engineering");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Materials Science and Engineering");
    return;
}

void filtersdialog_mitocw::on_mathsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Mathematics");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Mathematics");
    return;
}

void filtersdialog_mitocw::on_mechEngCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Mechanical Engineering");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Mechanical Engineering");
    return;
}

void filtersdialog_mitocw::on_mediaArtsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Media Arts and Sciences");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Media Arts and Sciences");
    return;
}

void filtersdialog_mitocw::on_artsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Music and Theater Arts");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Music and Theater Arts");
    return;
}

void filtersdialog_mitocw::on_nuclearEngCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Nuclear Science and Engineering");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Nuclear Science and Engineering");
    return;
}

void filtersdialog_mitocw::on_physicsCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Physics");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Physics");
    return;
}

void filtersdialog_mitocw::on_politicalScienceCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Political Science");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Political Science");
    return;
}

void filtersdialog_mitocw::on_sciTechSocietyCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Science, Technology, and Society");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Science, Technology, and Society");
    return;
}

void filtersdialog_mitocw::on_managementCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Sloan School of Management");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Sloan School of Management");
    return;
}

void filtersdialog_mitocw::on_supplementalResourcesCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Supplemental Resources");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Supplemental Resources");
    return;
}

void filtersdialog_mitocw::on_urbanStudiesCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Urban Studies and Planning");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Urban Studies and Planning");
    return;
}

void filtersdialog_mitocw::on_womenGenderStudiesCheckBox_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked)departments.append("Women's and Gender Studies");
    else if(arg1 == Qt::Unchecked)departments.removeOne("Women's and Gender Studies");
    return;
}
