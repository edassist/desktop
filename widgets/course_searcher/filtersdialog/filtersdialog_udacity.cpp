#include "filtersdialog_udacity.h"
#include "ui_filtersdialog_udacity.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QDebug>

filtersdialog_udacity::filtersdialog_udacity(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::filtersdialog_udacity)
{
    ui->setupUi(this);

    paid=false;
    free=false;
}

filtersdialog_udacity::~filtersdialog_udacity()
{
    delete ui;
}

QJsonObject filtersdialog_udacity::getData()
{
    filters={};

    QJsonArray schoolArray;
    QStringList :: iterator p;
    for (p=schools.begin();p!=schools.end();++p) schoolArray.append(QJsonValue(*p));

    QJsonArray difficultyArray;
    QList<int> ::iterator q;
    for (q=difficulty.begin();q!=difficulty.end();++q) difficultyArray.append(QJsonValue(*q));

    if(!schoolArray.empty()) filters.insert("school",schoolArray);
    if(!difficultyArray.empty())filters.insert("difficulty",difficultyArray);

    bool temp;
    if(!((paid && free)||(!paid && !free))) {
        if (paid) temp=true;
        else temp=false;
        filters.insert("paid",temp);
    }
    return filters;
}

void filtersdialog_udacity::on_schoolDataScienceCheckBox_stateChanged(int arg1)
{
    qDebug()<<"Hey There";
    if (arg1 == Qt::Checked) schools.append("School of Data Science");
    if (arg1 == Qt::Unchecked) schools.removeOne("School of Data Science");
    return;
}

void filtersdialog_udacity::on_schoolBusinessCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) schools.append("School of Business");
    if (arg1 == Qt::Unchecked) schools.removeOne("School of Business");
    return;
}

void filtersdialog_udacity::on_schoolAutonomousSystemsCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) schools.append("School of Autonomous Systems");
    if (arg1 == Qt::Unchecked) schools.removeOne("School of Autonomous Systems");
    return;
}

void filtersdialog_udacity::on_schoolCloudComputingCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) schools.append("School of Cloud Computing");
    if (arg1 == Qt::Unchecked) schools.removeOne("School of Cloud Computing");
    return;
}

void filtersdialog_udacity::on_schoolArtificialIntelligenceCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) schools.append("School of Artificial Intelligence");
    if (arg1 == Qt::Unchecked) schools.removeOne("School of Artificial Intelligence");
    return;
}

void filtersdialog_udacity::on_schoolCareerAdvancementCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) schools.append("Career Advancement");
    if (arg1 == Qt::Unchecked) schools.removeOne("Career Advancement");
    return;
}

void filtersdialog_udacity::on_schoolProgrammingCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) schools.append("School of Programming");
    if (arg1 == Qt::Unchecked) schools.removeOne("School of Programming");
    return;
}

void filtersdialog_udacity::on_beginnerCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) difficulty.append(1);
    if (arg1 == Qt::Unchecked) difficulty.removeOne(1);
    return;
}

void filtersdialog_udacity::on_intermediateCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) difficulty.append(2);
    if (arg1 == Qt::Unchecked) difficulty.removeOne(2);
    return;
}

void filtersdialog_udacity::on_advancedCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) difficulty.append(3);
    if (arg1 == Qt::Unchecked) difficulty.removeOne(3);
    return;
}

void filtersdialog_udacity::on_freeCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) free = true;
    if (arg1 == Qt::Unchecked) free = false;
    return;
}

void filtersdialog_udacity::on_paidCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) paid = true;
    if (arg1 == Qt::Unchecked) paid = false;
    return;
}

void filtersdialog_udacity::on_pushButton_clicked()
{
    //uncheck everything
    //paid/free
    ui->freeCheckBox->setCheckState(Qt::Unchecked);
    ui->paidCheckBox->setCheckState(Qt::Unchecked);

    //school
    ui->schoolBusinessCheckBox->setCheckState(Qt::Unchecked);
    ui->schoolDataScienceCheckBox->setCheckState(Qt::Unchecked);
    ui->schoolProgrammingCheckBox->setCheckState(Qt::Unchecked);
    ui->schoolCloudComputingCheckBox->setCheckState(Qt::Unchecked);
    ui->schoolAutonomousSystemsCheckBox->setCheckState(Qt::Unchecked);
    ui->schoolCareerAdvancementCheckBox->setCheckState(Qt::Unchecked);
    ui->schoolArtificialIntelligenceCheckBox->setCheckState(Qt::Unchecked);

    //difficulty
    ui->beginnerCheckBox->setCheckState(Qt::Unchecked);
    ui->advancedCheckBox->setCheckState(Qt::Unchecked);
    ui->intermediateCheckBox->setCheckState(Qt::Unchecked);

    return;
}
