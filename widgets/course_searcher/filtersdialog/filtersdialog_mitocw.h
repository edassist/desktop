#ifndef FILTERSDIALOG_MITOCW_H
#define FILTERSDIALOG_MITOCW_H

#include <QWidget>
#include <QJsonObject>

namespace Ui {
class filtersdialog_mitocw;
}

class filtersdialog_mitocw : public QWidget
{
    Q_OBJECT

public:
    explicit filtersdialog_mitocw(QWidget *parent = nullptr);
    ~filtersdialog_mitocw();

    QJsonObject getData();

private slots:
    void on_pushButton_clicked();

    void on_aeronauticsCheckBox_stateChanged(int arg1);

    void on_completeVideoCheckBox_stateChanged(int arg1);

    void on_completeAudioCheckBox_stateChanged(int arg1);

    void on_otherVideoCheckBox_stateChanged(int arg1);

    void on_otherAudioCheckBox_stateChanged(int arg1);

    void on_onlineTextbooksCheckBox_stateChanged(int arg1);

    void on_completeLecturesCheckBox_stateChanged(int arg1);

    void on_assessmentSolutionsCheckBox_stateChanged(int arg1);

    void on_studentProjectsCheckBox_stateChanged(int arg1);

    void on_instructorInsightsCheckBox_stateChanged(int arg1);

    void on_undergraduateCheckBox_stateChanged(int arg1);

    void on_graduateCheckBox_stateChanged(int arg1);

    void on_anthropologyCheckBox_stateChanged(int arg1);

    void on_architectureCheckBox_stateChanged(int arg1);

    void on_PECheckBox_stateChanged(int arg1);

    void on_biologicalEngCheckBox_stateChanged(int arg1);

    void on_biologyCheckBox_stateChanged(int arg1);

    void on_cognitiveSciencesCheckBox_stateChanged(int arg1);

    void on_chemicalEngCheckBox_stateChanged(int arg1);

    void on_chemistryCheckBox_stateChanged(int arg1);

    void on_civilEnvironmentalEngCheckBox_stateChanged(int arg1);

    void on_comparitiveMediaCheckBox_stateChanged(int arg1);

    void on_concourseCheckBox_stateChanged(int arg1);

    void on_planetarySciencesCheckBox_stateChanged(int arg1);

    void on_economicsCheckBox_stateChanged(int arg1);

    void on_edgertonCenterCheckBox_stateChanged(int arg1);

    void on_electricalCSEngCheckBox_stateChanged(int arg1);

    void on_engineeringSystemsCheckBox_stateChanged(int arg1);

    void on_experimentalStudyGroupCheckBox_stateChanged(int arg1);

    void on_globalStudiesCheckBox_stateChanged(int arg1);

    void on_healthSciencesCheckBox_stateChanged(int arg1);

    void on_historyCheckBox_stateChanged(int arg1);

    void on_dataSystemsCheckBox_stateChanged(int arg1);

    void on_philosphyCheckBox_stateChanged(int arg1);

    void on_literatureCheckBox_stateChanged(int arg1);

    void on_materialScienceEngCheckBox_stateChanged(int arg1);

    void on_mathsCheckBox_stateChanged(int arg1);

    void on_mechEngCheckBox_stateChanged(int arg1);

    void on_mediaArtsCheckBox_stateChanged(int arg1);

    void on_artsCheckBox_stateChanged(int arg1);

    void on_nuclearEngCheckBox_stateChanged(int arg1);

    void on_physicsCheckBox_stateChanged(int arg1);

    void on_politicalScienceCheckBox_stateChanged(int arg1);

    void on_sciTechSocietyCheckBox_stateChanged(int arg1);

    void on_managementCheckBox_stateChanged(int arg1);

    void on_supplementalResourcesCheckBox_stateChanged(int arg1);

    void on_urbanStudiesCheckBox_stateChanged(int arg1);

    void on_womenGenderStudiesCheckBox_stateChanged(int arg1);

private:
    Ui::filtersdialog_mitocw *ui;

    QStringList departments;
    QStringList levels;
    bool completeVideo;
    bool completeAudio;
    bool otherVideo;
    bool otherAudio;
    bool onlineTextbooks;
    bool completeLectures;
    bool assessmentsSolutions;
    bool studentProjects;
    bool instructorInsights;
    QJsonObject filters;
};

#endif // FILTERSDIALOG_MITOCW_H
