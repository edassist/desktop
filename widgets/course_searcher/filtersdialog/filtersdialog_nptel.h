#ifndef FILTERSDIALOG_NPTEL_H
#define FILTERSDIALOG_NPTEL_H

#include <QWidget>
#include <QJsonObject>

namespace Ui {
class filtersdialog_nptel;
}

class filtersdialog_nptel : public QWidget
{
    Q_OBJECT

public:
    explicit filtersdialog_nptel(QWidget *parent = nullptr);
    ~filtersdialog_nptel();

    QJsonObject getdata();

private slots:
    void on_pushButton_clicked();

    void on_aerospaceEngCheckBox_stateChanged(int arg1);

    void on_agricultureCheckBox_stateChanged(int arg1);

    void on_architectureCheckBox_stateChanged(int arg1);

    void on_atmosphericScienceCheckBox_stateChanged(int arg1);

    void on_basicCoursesCheckBox_stateChanged(int arg1);

    void on_biotechCheckBox_stateChanged(int arg1);

    void on_chemicalEngCheckBox_stateChanged(int arg1);

    void on_chemistryBiochemistryCheckBox_stateChanged(int arg1);

    void on_civilEngCheckBox_stateChanged(int arg1);

    void on_csEngCheckBox_stateChanged(int arg1);

    void on_eeEngCheckBox_stateChanged(int arg1);

    void on_eceEngCheckBox_stateChanged(int arg1);

    void on_engDesignCheckBox_stateChanged(int arg1);

    void on_enviromentCheckBox_stateChanged(int arg1);

    void on_generalCheckBox_stateChanged(int arg1);

    void on_humanitiesCheckBox_stateChanged(int arg1);

    void on_managementCheckBox_stateChanged(int arg1);

    void on_mathsCheckBox_stateChanged(int arg1);

    void on_mechEngCheckBox_stateChanged(int arg1);

    void on_metallurgyCheckBox_stateChanged(int arg1);

    void on_miningEngCheckBox_stateChanged(int arg1);

    void on_multipdisciplinaryCheckBox_stateChanged(int arg1);

    void on_nanotechCheckBox_stateChanged(int arg1);

    void on_oceanEngCheckBox_stateChanged(int arg1);

    void on_physicsCheckBox_stateChanged(int arg1);

    void on_specialSeriesCheckBox_stateChanged(int arg1);

    void on_textileEngheCckBox_stateChanged(int arg1);

    void on_videoCheckBox_stateChanged(int arg1);

    void on_webCheckBox_stateChanged(int arg1);

    void on_iiscCheckBox_stateChanged(int arg1);

    void on_iitBCheckBox_stateChanged(int arg1);

    void on_iitDCheckBox_stateChanged(int arg1);

    void on_iitGcheckBox_stateChanged(int arg1);

    void on_iitKanpurCheckBox_stateChanged(int arg1);

    void on_iitKharagpurCheckBox_stateChanged(int arg1);

    void on_iitMCheckBox_stateChanged(int arg1);

    void on_iitRCheckBox_stateChanged(int arg1);

private:
    Ui::filtersdialog_nptel *ui;

    QStringList disciplineName;
    QStringList institutes;
    QStringList contentType;

    QJsonObject filters;

};

#endif // FILTERSDIALOG_NPTEL_H
