#include "filtersdialog.h"
#include "ui_filtersdialog.h"
#include "filtersdialog_mitocw.h"
#include "filtersdialog_nptel.h"
#include "filtersdialog_udacity.h"
#include <QDebug>
#include <QJsonObject>

filtersDialog::filtersDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::filtersDialog)
{
    ui->setupUi(this);

    widgetsShowing=0;
    nptelWidget = nullptr;
    mitocwWidget = nullptr;
    udacityWidget = nullptr;
    nptelFilters={};
    mitocwFilters={};
    udacityFilters={};

    ui->moreOptionLabel->hide();
    ui->moreOptionScrollArea->setWidgetResizable(true);
    ui->moreOptionScrollArea->hide();

}

filtersDialog::~filtersDialog()
{
    delete ui;
    if (nptelWidget != nullptr) nptelWidget->deleteLater();
    if (mitocwWidget != nullptr) mitocwWidget->deleteLater();
    if (udacityWidget != nullptr) udacityWidget->deleteLater();
}

void filtersDialog::on_nptelCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked){
        nptelWidget = new filtersdialog_nptel;
        ui->moreOptionLabel->show();
        ui->moreOptionScrollArea->show();
        ui->moreOptionVerticalLayout->addWidget(nptelWidget);
        incrementWidgetsShowing();
    }
    if(arg1 == Qt::Unchecked){
        nptelWidget->deleteLater();
        nptelWidget=nullptr;
        decrementWidgetsShowing();
    }
}

void filtersDialog::on_mitocwCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked){
        mitocwWidget= new filtersdialog_mitocw;
        ui->moreOptionLabel->show();
        ui->moreOptionScrollArea->show();
        ui->moreOptionVerticalLayout->addWidget(mitocwWidget);
        incrementWidgetsShowing();
    }
    if(arg1 == Qt::Unchecked){
        mitocwWidget->deleteLater();
        mitocwWidget=nullptr;
        decrementWidgetsShowing();
    }
}

void filtersDialog::on_udacityCheckBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked){
        udacityWidget= new filtersdialog_udacity;
        ui->moreOptionLabel->show();
        ui->moreOptionScrollArea->show();
        ui->moreOptionVerticalLayout->addWidget(udacityWidget);
        incrementWidgetsShowing();
    }
    if(arg1 == Qt::Unchecked){
        udacityWidget->deleteLater();
        udacityWidget=nullptr;
        decrementWidgetsShowing();
    }
}

void filtersDialog::on_applyPushButton_clicked()
{
    filters={};//initializing it to be empty. filterDialog is only created once by courseSearcher so this is necessary.
    if (ui->nptelCheckBox->checkState() == Qt::Checked){
        nptelFilters=nptelWidget->getdata();
        filters.insert("NPTEL",nptelFilters);
    }
    if (ui->mitocwCheckBox->checkState() == Qt::Checked){
        mitocwFilters=mitocwWidget->getData();
        filters.insert("MITOCW",mitocwFilters);
    }
    if (ui->udacityCheckBox->checkState() == Qt::Checked){
        udacityFilters=udacityWidget->getData();
        filters.insert("UDACITY",udacityFilters);
    }

    emit updateFilters(filters);
    accept();
    return;
}

void filtersDialog::on_cancelPushButton_clicked()
{
    reject();
    return;
}

void filtersDialog::incrementWidgetsShowing()
{
    if(widgetsShowing == 0) this->showMaximized();
    ++widgetsShowing;
    return;
}

void filtersDialog::decrementWidgetsShowing()
{
    --widgetsShowing;
    if(widgetsShowing == 0){
        this->showNormal();
        ui->moreOptionLabel->hide();
        ui->moreOptionScrollArea->hide();
    }
    return;
}
