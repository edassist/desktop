#ifndef FILTERSDIALOG_H
#define FILTERSDIALOG_H

#include <QDialog>
#include <QJsonObject>
#include "filtersdialog_mitocw.h"
#include "filtersdialog_nptel.h"
#include "filtersdialog_udacity.h"

namespace Ui {
class filtersDialog;
}

class filtersDialog : public QDialog
{
    Q_OBJECT

public:
    explicit filtersDialog(QWidget *parent = nullptr);
    ~filtersDialog();

private slots:
    void on_nptelCheckBox_stateChanged(int arg1);

    void on_mitocwCheckBox_stateChanged(int arg1);

    void on_udacityCheckBox_stateChanged(int arg1);

    void on_applyPushButton_clicked();

    void on_cancelPushButton_clicked();

signals:
    void updateFilters(QJsonObject);

private:
    Ui::filtersDialog *ui;

    filtersdialog_nptel *nptelWidget;
    filtersdialog_mitocw *mitocwWidget;
    filtersdialog_udacity *udacityWidget;

    QJsonObject nptelFilters;
    QJsonObject mitocwFilters;
    QJsonObject udacityFilters;
    QJsonObject filters;

    int widgetsShowing;//determines if the moreOptionLabel is to be shown or not. its shown when this is >0.

    //private functions
    void incrementWidgetsShowing();//increases widgetsShowing and carries out certain actions if it is 0.
    void decrementWidgetsShowing();//decreases widgetsShowing and carries out certain actions if it reaches 0.
};

#endif // FILTERSDIALOG_H
