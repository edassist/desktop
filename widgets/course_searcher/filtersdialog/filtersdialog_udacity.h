#ifndef FILTERSDIALOG_UDACITY_H
#define FILTERSDIALOG_UDACITY_H

#include <QWidget>
#include <QJsonObject>

namespace Ui {
class filtersdialog_udacity;
}

class filtersdialog_udacity : public QWidget
{
    Q_OBJECT

public:
    explicit filtersdialog_udacity(QWidget *parent = nullptr);
    ~filtersdialog_udacity();

    QJsonObject getData();

private slots:
    void on_schoolDataScienceCheckBox_stateChanged(int arg1);

    void on_schoolBusinessCheckBox_stateChanged(int arg1);

    void on_schoolAutonomousSystemsCheckBox_stateChanged(int arg1);

    void on_schoolCloudComputingCheckBox_stateChanged(int arg1);

    void on_schoolArtificialIntelligenceCheckBox_stateChanged(int arg1);

    void on_schoolCareerAdvancementCheckBox_stateChanged(int arg1);

    void on_schoolProgrammingCheckBox_stateChanged(int arg1);

    void on_beginnerCheckBox_stateChanged(int arg1);

    void on_intermediateCheckBox_stateChanged(int arg1);

    void on_advancedCheckBox_stateChanged(int arg1);

    void on_freeCheckBox_stateChanged(int arg1);

    void on_paidCheckBox_stateChanged(int arg1);

    void on_pushButton_clicked();
    
private:
    Ui::filtersdialog_udacity *ui;

    QStringList schools;
    bool paid;
    bool free;
    QList <int> difficulty;

    QJsonObject filters;
};

#endif // FILTERSDIALOG_UDACITY_H
