#ifndef WELCOME_H
#define WELCOME_H

#include <QWidget>
#include "model/model.h"
#include "widgets/search_result/search_result.h"

namespace Ui {
class welcome;
}

class welcome : public QWidget
{
    Q_OBJECT

public:
    explicit welcome(QWidget *parent = nullptr);
    ~welcome();

signals:
    void searchTime();

    void studyTimeWelcome(QString, QJsonObject *,bool);//Welcome is to differentiate the signal from studyTime in MainWindow

    void reviseTime(QJsonObject* );

public slots:
    void reloadRegisteredCourses();//deletes everything in searchResultWidgets and calls loadRegisteredCourses

private slots:
    void on_courseSearcherPushButton_clicked();

    void on_studyYoutubePushButton_clicked();

    void continueLearningSlot(QJsonObject *information); //for emitting the studyTimeWelcome

    void deleteSearchResult(int position);//deletes searchResultWidget when user presses delete course button

    void reviseTimeSlot(QJsonObject *information);//for emitting reviseTime to mainwindow

private:
    Ui::welcome *ui;

    QMap <int, search_result *> * searchResultWidgets;
    search_result* searchResultWidget;

    //private functions
    void loadRegisteredCourses();//loads the registered courses into searchResultWidgets and displays the results.
};

#endif // WELCOME_H
