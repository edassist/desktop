#include "welcome.h"
#include "ui_welcome.h"

welcome::welcome(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::welcome)
{
    ui->setupUi(this);
    searchResultWidgets = new QMap <int, search_result *> ;
    ui->studyYoutubePushButton->setEnabled(false);

    loadRegisteredCourses();
}

welcome::~welcome()
{
    delete ui;

    QMap <int, search_result*> ::iterator p;
    for (p=searchResultWidgets->begin();p!=searchResultWidgets->end();++p) {
        (*p)->deleteLater();
    }
    searchResultWidgets->clear();
    delete  searchResultWidgets;
}

void welcome::on_courseSearcherPushButton_clicked()
{
    emit searchTime();
}

void welcome::on_studyYoutubePushButton_clicked()
{
    QJsonObject *empty; //essentially empty json object
    empty = new QJsonObject();
    empty->insert("course_provider","YOUTUBE");
    emit studyTimeWelcome("Youtube",empty,true);
}

void welcome::continueLearningSlot(QJsonObject *information)
{
    emit studyTimeWelcome(information->value("subjectname").toString(),information,false);
}

void welcome::deleteSearchResult(int index)
{
    searchResultWidgets->value(index)->deleteLater();
    searchResultWidgets->remove(index);

    //updating all the indexes greater than 'index' because indexes of all tabs after deleted tab are decreased by one
    QMap <int,search_result*> temp;
    QMap <int,search_result*>::iterator p;
    for (p=searchResultWidgets->begin();p!=searchResultWidgets->end();++p) {
        if(p.key()>index){
            temp.insert(p.key()-1,p.value());
            (*p)->setIndex(p.key()-1);
        }
        else temp.insert(p.key(),p.value());
    }
    searchResultWidgets->swap(temp);
    return;
}

void welcome::reviseTimeSlot(QJsonObject *information)
{
    emit reviseTime(information);
}

void welcome::loadRegisteredCourses()
{
    //fetch registered courses
    QJsonObject info = getRegisteredCourses();

    //use the search_result widget to display these courses

    //first nptel
    QJsonObject temp;
    QJsonArray tempArray = info.value("NPTEL").toArray();
    QJsonArray :: iterator p;
    for (p=tempArray.begin();p!=tempArray.end();++p){
        temp = p->toObject();
        temp.insert("course_provider","NPTEL");
        searchResultWidget = new search_result(this);
        searchResultWidget->populateInfo(temp);
        searchResultWidget->changePushButtonText("Continue Learning");
        searchResultWidget->addDateStarted(temp.value("creation_date").toString());
        searchResultWidget->setIndex(searchResultWidgets->size());

        //some connections
        connect(searchResultWidget,&search_result::continueLearning,this,&welcome::continueLearningSlot);
        connect(searchResultWidget,&search_result::requestDelete,this,&welcome::deleteSearchResult);
        connect(searchResultWidget,&search_result::reviseTime,this,&welcome::reviseTimeSlot);
        searchResultWidgets->insert(searchResultWidgets->size(),searchResultWidget);
    }

    //then youtube

    //now we display the searchResultWidgets
    QMap <int, search_result*> ::iterator q;
    for (q=searchResultWidgets->begin();q!=searchResultWidgets->end();++q) {
        ui->searchResultVerticalLayout->addWidget(*q);
    }
}

void welcome::reloadRegisteredCourses()
{
    //delete everything in searchResultWidgets
    QMap <int, search_result*> ::iterator p;
    for (p=searchResultWidgets->begin();p!=searchResultWidgets->end();++p) {
        (*p)->deleteLater();
    }
    searchResultWidgets->clear();

    loadRegisteredCourses();
    return;
}
