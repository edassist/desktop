#ifndef SEARCH_RESULT_H
#define SEARCH_RESULT_H

#include <QWidget>
#include <QJsonObject>
#include <QLabel>
#include <QMessageBox>

#include "widgets/course_searcher/learn_more/learn_more.h"
#include "model/model.h"

namespace Ui {
class search_result;
}

class search_result : public QWidget
{
    Q_OBJECT

public:
    explicit search_result(QWidget *parent = nullptr);
    ~search_result();

    void populateInfo(QJsonObject information);

    //needed by welcome widgets that use this class
    void changePushButtonText(QString);
    void addDateStarted(QString); //adds labels in dateStartedHorizontalLayout
    void setIndex(int position);

    //used by lecture selection widget
    void setupLectureSelection(int lectureNumber, QString title, QJsonObject *information);//sets up this widget as per the needs of lecture selection

private slots:
    void on_learnMorePushButton_clicked();

    void on_revisePushButton_clicked();

    void on_deleteCoursePushButton_clicked();

signals :
    void changeLearnMore(QJsonObject *information);//information is info variable

    //used by welcome widget
    void continueLearning(QJsonObject *information);//used by the welcome widgets that use this class
    void reviseTime(QJsonObject *information);
    void requestDelete(int position);//requests delete from welcome widget

    //used by lectureSelection
    void reviseLecture(QJsonObject *information);

private:
    Ui::search_result *ui;
    QString *courseProvider;
    QJsonObject info; //this contains the individual search result information
    QLabel *dateStarted;//just says "Date Started"
    QLabel *dateStartedValue;//holds the dateStarted
    int index; //holds the index of this widget in the searchResultWidgets list maintained in the welcome widget.
};

#endif // SEARCH_RESULT_H
