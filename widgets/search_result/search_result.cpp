#include "search_result.h"
#include "ui_search_result.h"

//search_result is the widget used to display individual search result for course_searcher , welcome and lectureSelection widgets.
search_result::search_result(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::search_result)
{
    ui->setupUi(this);
    courseProvider = new QString("NULL");
    ui->courseCategoryLabel->setText("Course Name");
    dateStarted = new QLabel(this);
    dateStartedValue = new QLabel(this);

    //hidden by default
    ui->revisePushButton->hide();
    ui->deleteCoursePushButton->hide();
}

search_result::~search_result()
{
    delete courseProvider;
    delete ui;
}

void search_result::populateInfo(QJsonObject information)
{
    info=information;
    delete courseProvider; //deleting the NULL courseProvider data.
    courseProvider = new QString(info.value("course_provider").toString());
    ui->groupBox->setTitle(*courseProvider);
    if (*courseProvider == "MITOCW"){
        ui->deptCategoryLabel->setText("Department");
        ui->deptNameLabel->setText(info.value("department").toString());
        ui->courseTitleLabel->setText(info.value("title").toString());
    }
    else if (*courseProvider=="NPTEL"){
        ui->deptCategoryLabel->setText("Discipline Name");
        ui->deptNameLabel->setText(info.value("disciplinename").toString());
        ui->courseTitleLabel->setText(info.value("subjectname").toString());
    }
    else if (*courseProvider == "UDACITY"){
        ui->deptCategoryLabel->setText("School");
        ui->deptNameLabel->setText(info.value("school").toString());
        ui->courseTitleLabel->setText(info.value("title").toString());
    }
}

void search_result::changePushButtonText(QString text)
{
    ui->learnMorePushButton->setText(text);
    //some other stuff
    ui->revisePushButton->show();
    ui->deleteCoursePushButton->show();
    return;
}

void search_result::addDateStarted(QString text)
{
    dateStartedValue->setText(text);
    dateStartedValue->setAlignment(Qt::AlignLeft);
    dateStartedValue->setAlignment(Qt::AlignVCenter);
    dateStartedValue->setSizePolicy(QSizePolicy::Expanding , QSizePolicy::Preferred);
    dateStarted->setSizePolicy(QSizePolicy::Minimum , QSizePolicy::Preferred);
    dateStarted->setText("Date Started");
    //making dateStarted bold
    QFont font = dateStarted->font();
    font.setBold(true);
    dateStarted->setFont(font);

    ui->dateStartedHorizontalLayout->addWidget(dateStarted);
    ui->dateStartedHorizontalLayout->addWidget(dateStartedValue);
    return;
}

void search_result::setIndex(int index)
{
    this->index = index ;
    return;
}

void search_result::setupLectureSelection(int lectureNumber, QString title, QJsonObject *information)
{
    ui->groupBox->setTitle(title);
    ui->courseCategoryLabel->setText("Lecture Number: ");
    ui->courseTitleLabel->setText(QString::number(lectureNumber));
    ui->deptCategoryLabel->setText("Title: ");
    ui->deptNameLabel->setText(title);
    ui->learnMorePushButton->setText("Revise");
    info = *information;
}

void search_result::on_learnMorePushButton_clicked()
{
    //signal is used according the class(welcome or course_searcher) using this class(search_result)
    emit changeLearnMore(&info); //this is used by course_searcher class
    emit continueLearning(&info); //this is used by welcome widget class
    emit reviseLecture(&info);//this is used by lectureSelection class
}

void search_result::on_revisePushButton_clicked()
{
    emit reviseTime(&info);
}

void search_result::on_deleteCoursePushButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Do you really want to delete this course ? ");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    QPushButton *deleteFiles = msgBox.addButton("Delete All Files Also",QMessageBox::DestructiveRole);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret = msgBox.exec();

    switch (ret){
        case QMessageBox::Yes :
            //check the type of course
            if(*courseProvider == "NPTEL"){
                removeCourseNptel(info.value("subjectid").toString());
                emit requestDelete(index);
            }
            break;
        case QMessageBox::No :
            //do nothing
            break;
        default :
            //for the case user presses third option
            if(msgBox.clickedButton() == deleteFiles){
                //check the type of course
                if(*courseProvider == "NPTEL"){
                    removeCourseNptel(info.value("subjectid").toString());
                    if (info.value("course_path")!=""){//for the case the use started the course but didn't choose a folder
                        QDir folder (info.value("course_path").toString());
                        folder.removeRecursively();
                    }
                    emit requestDelete(index);
                }
            }
            break;
    }

}
