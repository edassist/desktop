#ifndef STUDY_YOUTUBE_H
#define STUDY_YOUTUBE_H

#include <QWidget>
#include "study.h"

namespace Ui {
class study_youtube;
}

class study_youtube : public study
{
    Q_OBJECT

public:
    explicit study_youtube(QString *host, QNetworkAccessManager *netManager, QWidget *parent = nullptr, QJsonObject *information=nullptr, bool networkRequestNeeded = true);
    ~study_youtube();

private slots:
    void on_loadVideoButton_clicked();

    void on_videoUrlLineEdit_returnPressed();

    void on_loadUrlButton_clicked();

    void loadVideoSlot(); //used when loadVideoButton is pressed but mainWindow is not fullscreen

    void on_pushButton_clicked();

private:
    Ui::study_youtube *ui;
    void networkRequestFinished() override;
    void startNotes(QJsonObject lectureInfo) override;
};

#endif // STUDY_YOUTUBE_H
