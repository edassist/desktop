#ifndef LOADINGDIALOG_H
#define LOADINGDIALOG_H

#include <QDialog>

namespace Ui {
class loadingDialog;
}

class loadingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit loadingDialog(QWidget *parent = nullptr);
    ~loadingDialog();

public slots:
    void closeRequested();//catches signal from parent widget

private:
    Ui::loadingDialog *ui;
};

#endif // LOADINGDIALOG_H
