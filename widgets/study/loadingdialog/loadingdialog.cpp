#include "loadingdialog.h"
#include "ui_loadingdialog.h"

loadingDialog::loadingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::loadingDialog)
{
    ui->setupUi(this);
    setWindowTitle("Loading ... ");
}

loadingDialog::~loadingDialog()
{
    delete ui;
}

void loadingDialog::closeRequested()
{
    //closing dialog
    done(0);
}
