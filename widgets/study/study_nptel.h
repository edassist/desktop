#ifndef STUDY_NPTEL_H
#define STUDY_NPTEL_H

#include <QJsonObject>
#include <QTimer>
#include <QDateTime>
#include "study.h"
#include "lecture/lecture.h"
#include "model/model.h"

namespace Ui {
class study_nptel;
}

class study_nptel : public study
{
    Q_OBJECT

public:
    //networkRequestNeeded tells whether a network request is need to get lecture and extra_download info
    //if its false the info is gotten from the locally available database
    explicit study_nptel(QString *host, QNetworkAccessManager *netManager, QWidget *parent = nullptr,QJsonObject *information = nullptr, bool networkRequestNeeded = true);
    ~study_nptel();

private slots:
    void changeLectureInfo(QJsonObject lectureInfo);

    void on_mainIdeasDownloadPushButton_clicked();

    void on_captionSearchLineEdit_returnPressed();

    void on_captionFindNextPushButton_clicked();

    void on_captionClearPushButton_clicked();

    void on_cleanNotesPushButton_clicked();

    void on_clearNotesPushButton_clicked();

    void on_openNotesPushButton_clicked();

private:
    Ui::study_nptel *ui;
    QList <lecture *>lectureWidgets;
    QJsonObject currentLectureInfo;

    //private functions
    void networkRequestFinished() override;
    void startNotes(QJsonObject lectureInfo) override;
    void enableMainIdeasDownloadPushButton();

    void loadLecturesAndDownloads();//called after extraInfo has been fetched
};

#endif // STUDY_H
