#include "lecture.h"
#include "ui_lecture.h"

lecture::lecture(QJsonObject information, QNetworkAccessManager *netManager, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::lecture)
{
    ui->setupUi(this);
    lectureInfo = information;
    videoThumbnail = QPixmap (":/images/loading.jpg");//temporary till the time the thumbnail isn't downloaded
    videoThumbnail=videoThumbnail.scaled(100,100);

    ui->lectureNumberLabel->setText(QString::number(lectureInfo.value("lecture_number").toInt()));
    ui->videoThumbnailLabel->setPixmap(videoThumbnail);
    ui->titleLabel->setText(lectureInfo.value("title").toString());

    netReply=nullptr;
    this->netManager = netManager;
    networkDataBuffer = new QByteArray;

    //disabling pushbuttons on the basis on lectureInfo
    if(lectureInfo.value("pdf_download").toString() == "PDF unavailable") ui->pdfLectureDownloadPushButton->setDisabled(true);
    if(lectureInfo.value("video_download").toString() == "")ui->lectureVideoDownloadPushButton->setDisabled(true);

    fetchVideoThumbnail();

    //connections
    connect(this,&lecture::updateStartedSignal,this,&lecture::updateStartedSlot);
}

lecture::~lecture()
{
    delete networkDataBuffer;
    delete ui;
}

void lecture::fetchVideoThumbnail()
{
    QNetworkRequest request;
    request.setHeader(QNetworkRequest::UserAgentHeader,QVariant(QString("EdAssist-Qt")));
    request.setUrl(QUrl("https://i.ytimg.com/vi/"+lectureInfo.value("youtube_id").toString()+"/mqdefault.jpg"));
    QUrl url = request.url();
    netReply=netManager->get(request);

    connect(netReply,&QNetworkReply::finished,this,&lecture::networkRequestFinished);
    connect(netReply,&QIODevice::readyRead,this,&lecture::networkDataReady);
}

void lecture::networkRequestFinished()
{
    //checking for network errors.
    if (netReply->error() != QNetworkReply::NoError){
        emit displayError("Thumbnail could not be fetched an error occurred for lecture number"+lectureInfo.value("lecture_number").toInt(),3000);
        netReply->abort();
    }
    else {
        videoThumbnail.loadFromData(*networkDataBuffer);
        videoThumbnail = videoThumbnail.scaled(200,100);
        ui->videoThumbnailLabel->setPixmap(videoThumbnail);
    }
    networkDataBuffer->clear();
    netReply->deleteLater();
}

void lecture::networkDataReady()
{
    networkDataBuffer->append(netReply->readAll());
}


void lecture::on_viewLecturePushButton_clicked()
{
    emit changeVideo(lectureInfo);
    emit updateStartedSignal();
}

void lecture::updateStartedSlot()
{
    if(!updateStartedNptel(lectureInfo.value("subjectid_id").toString(), lectureInfo.value("lecture_number").toInt())){
        qDebug()<<"Unable to update 'started' attribute in lectures table for subjectid"<<lectureInfo.value("subjectid_id").toString()<<" and lecture_number"<< lectureInfo.value("lecture_number").toInt();
    }
}
