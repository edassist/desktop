#ifndef LECTURE_H
#define LECTURE_H

#include <QWidget>
#include <QJsonObject>
#include <QPixmap>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
#include "model/model.h"

namespace Ui {
class lecture;
}

class lecture : public QWidget
{
    Q_OBJECT

public:
    explicit lecture(QJsonObject information, QNetworkAccessManager *netManager, QWidget *parent = nullptr);
    ~lecture();

signals:
    void displayError(QString,int);//emitted when a network error occurs. Caught by study.cpp to display a message in status bar.

    void changeVideo(QJsonObject object);//emitted when user clicks the viewLecturePushButton. Starts notes, fetches subtitles, and loads youtube video.

    void updateStartedSignal();//emitted when user presses viewLecturePushButton.

private slots:
    void on_viewLecturePushButton_clicked();

    /*catches updateStartedSignal. Updates started attribute in lectures table.
    Using seperate signal so that database activities takes place in the background.*/
    void updateStartedSlot();

private:
    Ui::lecture *ui;
    QJsonObject lectureInfo;//contains info needed by the widget
    QPixmap videoThumbnail;

    //network stuff
    QNetworkAccessManager * netManager;
    QNetworkReply * netReply;
    QByteArray * networkDataBuffer;

    //private functions
    void fetchVideoThumbnail();

    void networkRequestFinished();
    void networkDataReady();
};

#endif // LECTURE_H
