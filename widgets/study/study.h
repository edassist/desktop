#ifndef STUDY_H
#define STUDY_H

#include <QWidget>
#include <QtXml>
#include <QtWebEngineWidgets/QtWebEngineWidgets>
#include <QProgressBar>
#include <QPushButton>
#include <QStatusBar>
#include <QNetworkAccessManager>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>

#include "model/model.h"
#include "widgets/study/webchannelObject.h"
#include "widgets/study/loadingdialog/loadingdialog.h"
#include "wheel_event_filter.h"


class study : public QWidget
{
    Q_OBJECT

public:
    explicit study(QString * host, QNetworkAccessManager *netManager, QWidget *parent = nullptr, QJsonObject *information = nullptr, bool networkRequestNeeded = true);
    ~study();

    void setIndex(int);//alters index

    QString* getLectureFolder();

    int getWebviewWidth();
    int getWebviewHeight();
    WId getWebviewWinId();

    void displayStatusBarMessage(QString,int);
    bool writeToSequenceFile(QString);//returns true if it was able to write to the sequence file

protected slots:
    //for the progress bar in the status bar
    void startLoading();
    void loading(int);
    void loaded(bool);

    void savesNotes();

    void loadVideo(QJsonObject lectureInfo);
    void registerCourseSlot();//for calling the registerCourse function from model.h

    void fetchCaptions(QJsonObject lectureInfo);//fetches the subtitles for the youtube video
    virtual void startNotes(QJsonObject lectureInfo) = 0;//initializes the note making machinery.
    //This is virtual because notesPlainTextEdit needs to be inserted back into the ui when expanded.

    QString readFile(QString); //reads the file in the argument and returns its contents in a string
    bool saveToFile(QString); //saves the contents of notesPlainTextEdit to the file in the argument

    void impWordsBrowserSourceChanged(QUrl src);
    void changeSubtitlesSlot(QString anchor);//catches the changeSubtitle signal from channelObject
    void captionsBrowserSourceChanged(QUrl src);
    void pauseSyncCaptions(); //sets pauseSync to true. called when user scrolls the captionBrowser
    void startSyncCaptions(); //sets pauseSync to false.

    void sendDataPoints();

signals:
    void goFullScreen();
    void registerCourseSignal();
    void closeLoadingDialog();
    void reloadWelcome();//emitted when either the user starts a new course or sets the lectureFolder
    //tells welcome widget to reload its registered course list

protected:
    //web stuff
    QWebEngineView *webview;
    QWebChannel *webchannel;//one and only webchannel between c++ and webview
    webchannelObject *channelObject; //this is the object which will be published over the webchannel

    //ui stuff
    QStatusBar *statusBar;
    QProgressBar *webViewProgressBar;
    QPlainTextEdit *notesPlainTextEdit;
    QTextBrowser *captionsBrowser;
    QTextBrowser *impWordsBrowser;
    QIcon *showMoreIcon;
    QIcon *showLessIcon;

    //network stuff
    QNetworkAccessManager * netManager;
    QNetworkReply * netReply;
    QByteArray * networkDataBuffer;
    QString * host;
    QJsonObject info;
    QJsonObject extraInfo; // all the other information about the course
    QStringList* dataToBeSent; //data that will be sent to webengineWidget

    //captions stuff
    QList <int>* queryCursorPositions; //stores the anchor positions of query in captionDocument
    QMap <QString, int>* captionCursorPositions; //map with key as the begin value and key as cursor position of a caption in captionDocument
    int currentCursorPositionIndex; //a index of the above list
    QTextDocument* captionDocument; //this text document is stored in the captionsBrowser
    QString captionQuery; //contains the current search query for captionsBrowser
    QTextCharFormat normalCharFormat; //normal format of characters in captionDocument
    QTextCharFormat* highlightFormatting; //text formating for highlighting results in captionDocument
    QTextCharFormat* currentResultFormatting; //text formatting for highlighting current result in captionDocument
    QTimer* pauseSyncTimer; //timer which handles after how long syncing of video and captions will start
    bool pauseSync; //if true captions will not be synced with video. used in changeSubtitlesSlot
    wheel_event_filter* wheelEventFilterObject;
    QString currentAnchor; //stores the name of the current anchor of captionBrowser.

    //other stuff
    int index; //holds the index number of the tab where this widget is put
    QString *currentNotesFile;
    QString lectureFolder; //this is path for this lecture
    QString courseFolder; //path for the whole course
    enum widgetState {expanded, collapsed}; //for widgets which can be expanded and collapsed. expanded means it can be collapsed.

    //private functions

    //notes stuff
    void setCurrentNotesFile(QString filename);
    QString getCourseFolder();//get the folder where screenshots and stuff is stored
    void setCourseFolder(); //makes the folder and updates it value in the database

    //captions stuff
    void findInCaptions (QString query); //searchs and highlights occurences of query string in captionsBrowser
    void clearSearchInCaptions (); //clears the highlighted elements in captionsBrowser

    //for obtaining data from internet different from chromium instance
    void searchRequest(QUrl);//issues a search request.
    void networkDataReady();// data ready to be fetched
    void networkDataFetching(int, int);//fetches data and puts it into a buffer
    void networkError();//slot for netReply::error
    virtual void networkRequestFinished() = 0;//child does whatever it wants to do with the networkDataBuffer
};

#endif // STUDY_H
