#include "study.h"

study::study(QString *host, QNetworkAccessManager *netManager, QWidget *parent, QJsonObject *information, bool networkRequestNeeded) :
    QWidget(parent)
{
    info=*information;
    this->host=host;
    this->netManager = netManager;

    webview=new QWebEngineView(this);
    //disabling local storage
    webview->page()->profile()->setHttpCacheType(QWebEngineProfile::MemoryHttpCache);
    webview->page()->profile()->setPersistentCookiesPolicy(QWebEngineProfile::NoPersistentCookies);
    webview->setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding);
    webview->setMinimumWidth(450);
    //setting up webchannel
    webchannel = new QWebChannel(webview->page());
    channelObject = new webchannelObject();
    webview->page()->setWebChannel(webchannel);
    webchannel->registerObject(QString("channelObject"),channelObject);

    netReply = nullptr;
    networkDataBuffer = new QByteArray();

    statusBar = new QStatusBar(this);
    statusBar->setSizeGripEnabled(false);
    statusBar->hide();

    webViewProgressBar = new QProgressBar(this);
    webViewProgressBar->hide();
    this->statusBar->addWidget(webViewProgressBar);

    showLessIcon = new QIcon(":/images/upArrow.jpeg");
    showMoreIcon= new QIcon(":/images/downArrow.jpeg");

    this->lectureFolder="NULL";
    if(info.contains("course_path")){
        /*info contains course_path when course has been started by the user
         * i.e. info is obtained from the local database */
        this->courseFolder = info.value("course_path").toString();
        if(this->courseFolder == "") {
            setCourseFolder();
        }
    }
    else {
        this->courseFolder = "NULL";
    }
    this->currentNotesFile=new QString("NULL");

    notesPlainTextEdit = new QPlainTextEdit();
    notesPlainTextEdit->setPlaceholderText("Your edits will be save automatically");
    notesPlainTextEdit->setReadOnly(true);
    impWordsBrowser = new QTextBrowser(this);
    impWordsBrowser->setReadOnly(true);
    dataToBeSent = new QStringList();

    //captions stuff
    queryCursorPositions = new QList<int>;
    captionCursorPositions = new QMap<QString, int>;
    highlightFormatting = new QTextCharFormat();
    currentResultFormatting = new QTextCharFormat();
    highlightFormatting->setBackground(QBrush("#cbf1f5"));
    currentResultFormatting->setBackground(QBrush("#a3f7bf"));
    wheelEventFilterObject = new wheel_event_filter(this);
    captionDocument = new QTextDocument;
    captionsBrowser = new QTextBrowser(this);
    captionsBrowser->setReadOnly(true);
    captionsBrowser->setDocument(captionDocument);
    captionsBrowser->viewport()->installEventFilter(wheelEventFilterObject);
    captionsBrowser->verticalScrollBar()->installEventFilter(wheelEventFilterObject);
    captionsBrowser->horizontalScrollBar()->installEventFilter(wheelEventFilterObject);
    pauseSyncTimer = new QTimer(this);
    pauseSync = false;
    currentAnchor = "";

    //connections
    connect(this,SIGNAL(goFullScreen()),parent,SLOT(showMaximized()));
    connect(this,SIGNAL(registerCourseSignal()),SLOT(registerCourseSlot()));
    connect(webview,SIGNAL(loadStarted()),SLOT(startLoading()));
    connect(webview,SIGNAL(loadProgress(int)),SLOT(loading(int)));
    connect(webview,SIGNAL(loadFinished(bool)),SLOT(loaded(bool)));
    connect(channelObject,&webchannelObject::changeSubtitles,this,&study::changeSubtitlesSlot);
    connect(notesPlainTextEdit,&QPlainTextEdit::textChanged,this,&study::savesNotes);
    connect(impWordsBrowser,&QTextBrowser::sourceChanged,this,&study::impWordsBrowserSourceChanged);
    connect(captionsBrowser,&QTextBrowser::sourceChanged,this,&study::captionsBrowserSourceChanged);
    connect(captionsBrowser->verticalScrollBar(),&QScrollBar::sliderMoved,this,&study::pauseSyncCaptions);
    connect(captionsBrowser->horizontalScrollBar(),&QScrollBar::sliderMoved,this,&study::pauseSyncCaptions);
    connect(wheelEventFilterObject,&wheel_event_filter::userScrolled,this,&study::pauseSyncCaptions);
    connect(pauseSyncTimer,&QTimer::timeout,this,&study::startSyncCaptions);
}

study::~study()
{
    //web stuff
    webview->page()->profile()->cookieStore()->deleteAllCookies();
    webview->deleteLater();
    webchannel->deleteLater();

    //uistuff
    webViewProgressBar->deleteLater();
    statusBar->deleteLater();
    notesPlainTextEdit->deleteLater();
    captionsBrowser->deleteLater();
    captionDocument->deleteLater();
    delete showLessIcon;
    delete showMoreIcon;

    //networking stuff
    delete networkDataBuffer;

    //other stuff;
    delete  currentNotesFile;
    if (!queryCursorPositions->isEmpty()){
        queryCursorPositions->clear();
        delete queryCursorPositions;
    }
}

void study::loadVideo(QJsonObject lectureInfo)//this function the current youtube video into a iframe
{
    webview->page()->runJavaScript(QString("player.loadVideoById('%1')").arg(lectureInfo.value("youtube_id").toString()));
    return;
}

void study::registerCourseSlot()
{
    QStringList l = QDateTime::currentDateTime().toString(Qt::ISODate).split('T');
    QString datetime = l[0] + " " + l[1];

    QJsonObject temp = info;
    temp.insert("creation_date",datetime);
    temp.insert("course_path",courseFolder);
    QJsonObject bigInfo = extraInfo;
    bigInfo.insert("courses",temp);

    //Loading messagebox while SQL queries are executed
    //so that user doesn't think there is a problem with the application
    loadingDialog dialog(this);
    connect(this,&study::closeLoadingDialog,&dialog,&loadingDialog::closeRequested);
    dialog.open();
    if(!registerCourse("NPTEL",bigInfo)){
        displayStatusBarMessage("ERROR: Unable to register course",5000);
    }
    emit reloadWelcome();
    emit closeLoadingDialog();
}

void study::fetchCaptions(QJsonObject lectureInfo)
{
    //for the time being only for nptel
    displayStatusBarMessage("Downloading Subtitles",5000);
    searchRequest(QUrl(QString("%1/search/nptel/%2/%3").arg(*host).arg(info.value("subjectid").toString())
                       .arg(lectureInfo.value("youtube_id").toString())));
}

QString study::getCourseFolder()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Folder for this course"),
                                                    "/home",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    //user should select a file
    while(dir.isEmpty()){
        QMessageBox::information(this,"Select Folder","Please select a folder to store the notes and screenshots");
        dir = QFileDialog::getExistingDirectory(this, tr("Open Folder to save notes and screenshots"),
                                                              "/home",
                                                              QFileDialog::ShowDirsOnly
                                                              | QFileDialog::DontResolveSymlinks);
    }
    //check if we have read write permissions for the directory selected
    QFileInfo directory(dir);
    while(!directory.isWritable()){
        QMessageBox::information(this,"No Write Permission","Please select another folder to store the notes and screenshots");
        dir = QFileDialog::getExistingDirectory(this, tr("Open Folder to save notes and screenshots"),
                                                              "/home",
                                                              QFileDialog::ShowDirsOnly
                                                              | QFileDialog::DontResolveSymlinks);
        while(dir.isEmpty()){
            QMessageBox::information(this,"Select Folder","Please select a folder to store the notes and screenshots");
            dir = QFileDialog::getExistingDirectory(this, tr("Open Folder to save notes and screenshots"),
                                                                  "/home",
                                                                  QFileDialog::ShowDirsOnly
                                                                  | QFileDialog::DontResolveSymlinks);
        }
        directory.setFile(dir);
    }

    return dir;
}

QString study::readFile(QString filename)
{
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        displayStatusBarMessage("ERROR : Unable to open "+filename,5000);
        return "ERROR";
    }

    QString  fileContent;
    QTextStream in(&file);
    QString line = in.readLine();
    while(!line.isNull()){
        fileContent.append(line);
        line = in.readLine();
    }
    file.close();

    return fileContent;
}

bool study::saveToFile(QString filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text )){
        displayStatusBarMessage("ERROR : Unable to open"+filename,5000);
        return false;
    }
    QTextStream out(&file);
    out<<notesPlainTextEdit->toPlainText();
    file.close();
    return true;
}

void study::impWordsBrowserSourceChanged(QUrl src)
{
    emit channelObject->seekTo(src.toString().replace("#",""));
}

void study::captionsBrowserSourceChanged(QUrl src)
{
    emit channelObject->seekTo(src.toString().replace("#",""));
}

void study::pauseSyncCaptions()
{
    pauseSync = true;
    pauseSyncTimer->start(5000);
}

void study::startSyncCaptions()
{
    pauseSync = false;
}

void study::sendDataPoints()
{
    emit channelObject->sendingDataPoints(*dataToBeSent);
}

bool study::writeToSequenceFile(QString filename)
{
    QFile file(lectureFolder+"/sequence.txt");
    if (!file.open(QIODevice::Append | QIODevice::Text )) return false;
    QTextStream out(&file);
    out<<filename;
    out<<"\n";
    file.close();
    return true;
}


void study::setIndex(int index)
{
    this->index=index;
    return;
}
QString* study::getLectureFolder()
{
    return &lectureFolder;
}

void study::setCurrentNotesFile(QString filename)
{
    delete this->currentNotesFile;
    this->currentNotesFile = new QString (filename);
    return;
}

void study::findInCaptions(QString query)
{
    if(captionQuery != query){ //when the user presses Find Next for a new word
        captionQuery = query;
        QTextCursor cursor = captionDocument->find(query); // returns a selection if a match is found
        QTextCursor cursor3(captionDocument);
        while(!cursor.isNull()){
            //selection returned by find sometimes has a extra space character in the end
            //so creating another selection using cursor3
            cursor3.setPosition(cursor.anchor());
            cursor3.movePosition(QTextCursor::EndOfWord,QTextCursor::KeepAnchor);
            cursor3.mergeCharFormat(*highlightFormatting);
            queryCursorPositions->append(cursor.anchor());
            cursor = captionDocument->find(query,cursor.position());
        }

        if (!queryCursorPositions->isEmpty()){
            //change higlighting for first result
            QTextCursor cursor2 (captionDocument);
            cursor2.setPosition(queryCursorPositions->at(0));
            cursor2.movePosition(QTextCursor::EndOfWord,QTextCursor::KeepAnchor);
            cursor2.setCharFormat(normalCharFormat);
            cursor2.mergeCharFormat(*currentResultFormatting);
            //set cursor to the first result
            cursor2.setPosition(queryCursorPositions->at(0));
            currentCursorPositionIndex = 0;
            captionsBrowser->setTextCursor(cursor2);
        }
    }
    else if(!queryCursorPositions->isEmpty()){ //no need to run it if no selections were found
        QTextCursor cursor(captionDocument);
        //set older current result to highlightFormatting
        cursor.setPosition(queryCursorPositions->at(currentCursorPositionIndex));
        cursor.movePosition(QTextCursor::EndOfWord,QTextCursor::KeepAnchor);
        cursor.setCharFormat(normalCharFormat);
        cursor.mergeCharFormat(*highlightFormatting);

        if(currentCursorPositionIndex == queryCursorPositions->size()-1) currentCursorPositionIndex = 0; //repeat from start
        else currentCursorPositionIndex++;
        //change higlighting for current result
        cursor.setPosition(queryCursorPositions->at(currentCursorPositionIndex));
        cursor.movePosition(QTextCursor::EndOfWord,QTextCursor::KeepAnchor);
        cursor.setCharFormat(normalCharFormat);
        cursor.mergeCharFormat(*currentResultFormatting);
        //setting cursor to the start of currentResult
        cursor.setPosition(queryCursorPositions->at(currentCursorPositionIndex));
        captionsBrowser->setTextCursor(cursor);
    }
}

void study::clearSearchInCaptions()
{
    if (queryCursorPositions->isEmpty()) return; //leave if nothing to clear
    QTextCursor cursor(captionDocument);
    QList<int> :: iterator p;
    for (p=queryCursorPositions->begin();p!=queryCursorPositions->end();++p){
        cursor.setPosition(*p);
        cursor.movePosition(QTextCursor::EndOfWord,QTextCursor::KeepAnchor);
        cursor.setCharFormat(normalCharFormat);
    }
    captionQuery = "";
    queryCursorPositions->clear();
}

int study::getWebviewWidth()
{
    return webview->width();
}

int study::getWebviewHeight()
{
    return webview->height();
}

WId study::getWebviewWinId()
{
    return webview->winId();
}

void study::displayStatusBarMessage(QString message,int timeout =0)
{
    statusBar->show();
    statusBar->showMessage(message,timeout);
    QTimer::singleShot(timeout,statusBar,SLOT(hide()));
}

void study::startLoading(){
    statusBar->show();
    this->webViewProgressBar->show();
}

void study::loading(int progress){
    this->webViewProgressBar->setValue(progress);
}

void study::loaded(bool ok){
    this->webViewProgressBar->hide();
    if (!ok) displayStatusBarMessage("Error occured while fetching data over internet",5000);
    else statusBar->hide();
}

void study::savesNotes()
{
    QString fileName;
    if (*(this->currentNotesFile) == "NULL"){
        fileName="Notes" + QDateTime::currentDateTime().toString(Qt::ISODate)+".txt";
        if(!writeToSequenceFile(fileName)){
            QMessageBox::critical(this,"ERROR","Unable to write to sequence file");
            return;
        }
        fileName=this->lectureFolder+"/"+fileName;
        setCurrentNotesFile(fileName);
    }
    else fileName = *(this->currentNotesFile);

    saveToFile(fileName);
    return;
}

void study::searchRequest(QUrl url)
{
    QNetworkRequest request;
    request.setHeader(QNetworkRequest::UserAgentHeader,QVariant(QString("EdAssist-Qt")));
    request.setUrl(url);
    netReply=netManager->get(request);

    statusBar->show();
    webViewProgressBar->show();
    connect(netReply,&QNetworkReply::finished,this,&study::networkRequestFinished);
    connect(netReply,&QIODevice::readyRead,this,&study::networkDataReady);
    connect(netReply,&QNetworkReply::downloadProgress,this,&study::networkDataFetching);

    return ;
}

void study::networkDataReady()
{
    networkDataBuffer->append(netReply->readAll());
}

void study::networkDataFetching(int bytesDownloaded, int bytesTotal)
{
    if (bytesTotal == 0) return;
    webViewProgressBar->setValue( (bytesDownloaded/bytesTotal)*100 );
    return;
}

void study::networkError()
{
    displayStatusBarMessage("Network Error",5000);
    //Debug()<<"ERROR";
}

void study::setCourseFolder()
{
    QString dir = getCourseFolder();
    this->courseFolder = dir + "/" + info.value("subjectname").toString();
    //creating the courseFolder
    QDir directory(dir);
    if(!directory.mkdir(info.value("subjectname").toString()) && !QFileInfo::exists(courseFolder)){
        QMessageBox::critical(this,"ERROR","Unable to make course folder. Close and reopen this tab");
    }
    else{
        updateCoursePath(courseFolder,info.value("subjectid").toString());
        emit reloadWelcome();
    }
    return;
}

void study::changeSubtitlesSlot(QString anchor)
{
    if (currentAnchor.isEmpty()){
        currentAnchor = anchor;
    }
    else{
        //setting old anchor's formatting to normal
        QTextCursor cursor(captionDocument);
        cursor.setPosition(captionCursorPositions->value(currentAnchor));
        cursor.movePosition(QTextCursor::EndOfBlock,QTextCursor::KeepAnchor); //one text block contains one caption
        cursor.setCharFormat(normalCharFormat);
        cursor.movePosition(QTextCursor::EndOfBlock); //so that the whole highlighted block is visible
        currentAnchor = anchor;
    }
    //setting new anchor's formatting
    QTextCharFormat captionFormatting;
    captionFormatting.setBackground(QBrush("#fff591"));

    QTextCursor cursor(captionDocument);
    cursor.setPosition(captionCursorPositions->value(anchor));
    cursor.movePosition(QTextCursor::EndOfBlock,QTextCursor::KeepAnchor);
    cursor.mergeCharFormat(captionFormatting);
    if (!pauseSync) { //restoring cursor's position only if pauseSync is false.
        cursor.movePosition(QTextCursor::EndOfBlock); //ensures ending of block is visible
        captionsBrowser->setTextCursor(cursor);
    }
}
