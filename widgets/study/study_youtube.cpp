#include "study_youtube.h"
#include "ui_study_youtube.h"

study_youtube::study_youtube(QString *host, QNetworkAccessManager *netManager, QWidget *parent, QJsonObject *information, bool networkRequestNeeded) :
    study(host,netManager, parent, information, networkRequestNeeded),
    ui(new Ui::study_youtube)
{
    ui->setupUi(this);
    ui->statusBarLayout->addWidget(statusBar);
    ui->webviewLayout->addWidget(webview);
    webview->setUrl(QUrl("https://youtube.com"));

}

study_youtube::~study_youtube()
{
    delete ui;
}

void study_youtube::on_loadVideoButton_clicked()
{
    if (!this->isMaximized()) {
        emit goFullScreen();
        QTimer::singleShot(1000,this,SLOT(loadVideoSlot()));
    }
    else {
        //loadVideo("abc");//extract videoId here
    }
    return;
}

void study_youtube::on_videoUrlLineEdit_returnPressed()
{
    QString userUrl=ui->videoUrlLineEdit->text();
    webview->load(userUrl);
    return;
}

void study_youtube::on_loadUrlButton_clicked()
{
    QString userUrl=ui->videoUrlLineEdit->text();
    webview->load(userUrl);
    return;
}

void study_youtube::loadVideoSlot()
{
    //loadVideo("abc");//extract videoId here
    return;
}

void study_youtube::on_pushButton_clicked()
{
    webview->back();
}

void study_youtube::networkRequestFinished()
{
    networkDataBuffer->clear();
    netReply->deleteLater();
}

void study_youtube::startNotes(QJsonObject lectureInfo)
{
    return ;
}

