#include "wheel_event_filter.h"

wheel_event_filter::wheel_event_filter(QObject *parent) : QObject(parent)
{

}

bool wheel_event_filter::eventFilter(QObject *watched, QEvent *event)
{
    if(event->type() == QEvent::Wheel) emit userScrolled();
    return QObject::eventFilter(watched, event);
}
