#ifndef WHEEL_EVENT_FILTER_H
#define WHEEL_EVENT_FILTER_H

#include <QObject>
#include <QEvent>

class wheel_event_filter : public QObject
{
    Q_OBJECT
public:
    explicit wheel_event_filter(QObject *parent = nullptr);

protected:
    bool eventFilter(QObject *watched, QEvent *event) override;

signals:
    void userScrolled(); //emitted Wheel event occurs

};

#endif // WHEEL_EVENT_FILTER_H
