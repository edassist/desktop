#include "study_nptel.h"
#include "ui_study_nptel.h"

study_nptel::study_nptel(QString *host, QNetworkAccessManager *netManager, QWidget *parent, QJsonObject *information, bool networkRequestNeeded) :
    study(host, netManager, parent, information, networkRequestNeeded),
    ui(new Ui::study_nptel)
{    
    ui->setupUi(this);
    ui->statusBarLayout->addWidget(statusBar);
    ui->lectureScrollArea->setWidgetResizable(true);
    ui->webviewLayout->addWidget(webview);
    ui->subtitlesVerticalLayout->addWidget(captionsBrowser);
    ui->importantWordsLayout->addWidget(impWordsBrowser);
    ui->notesVerticalLayout->addWidget(notesPlainTextEdit);

    if(networkRequestNeeded){
        //issue a request to get lectures and extra downloads info
        searchRequest(QUrl(QString("%1/search/nptel/%2").arg(*host).arg(info.value("subjectid").toString())));
    }
    else{
        extraInfo.insert("lectures",getLectureNptel(info.value("subjectid").toString()));
        extraInfo.insert("extra_downloads",getExtraDowloadsNptel(info.value("subjectid").toString()));
        loadLecturesAndDownloads();
    }

    //setting up webview
    QFile file (":/html/index.html");
    if(!file.open(QIODevice::ReadOnly | QIODevice ::Text)) return;
    QTextStream in(&file);
    QString html = in.readAll();
    webview->setHtml(html);
}

study_nptel::~study_nptel()
{
    delete ui;
}

void study_nptel::changeLectureInfo(QJsonObject lectureInfo)
{
    currentLectureInfo = lectureInfo;
}

void study_nptel::networkRequestFinished()
{
    QString path = netReply->url().path();
    if (path == QString("/search/nptel/%1").arg(info.value("subjectid").toString())){ // for lecture and downloads
        webViewProgressBar->hide();

        //checking for network errors
        if(netReply->error() != QNetworkReply::NoError) {
            displayStatusBarMessage("Some error occurred while fetching data",5000);
            netReply->abort();
        }
        else{
            statusBar->hide();
            QJsonDocument searchResults= QJsonDocument::fromJson(*networkDataBuffer);
            this->extraInfo=searchResults.object();
            loadLecturesAndDownloads();
            emit registerCourseSignal();
        }
    }
    else if(path.split("/").back() == "imp_words") { // for important_words
        if(netReply->error() != QNetworkReply::NoError){
            displayStatusBarMessage("Some error occured while downloading main ideas",5000);
            captionDocument->clear();
            captionDocument->setHtml("<h3>Some error Occurred</h3>");
            ui->mainIdeasDownloadPushButton->setDisabled(false);
            netReply->abort();
        }
        else{
            statusBar->hide();
            QJsonObject object = QJsonDocument::fromJson(*networkDataBuffer).object();
            QStringList mainIdeas = object.keys();
            //constructing the html document
            QString html("<head><style type='text/css'>a {color: #903749;\
                         text-decoration: none; }</style></head><body>");
            for (QStringList::iterator p=mainIdeas.begin(); p!=mainIdeas.end(); ++p){
                html.append(QString("<a name=%1 href='#%1'>%2</a><br/>").arg(object.value(*p).toString()).arg(*p));
            }
            html.append("</body>");
            impWordsBrowser->clear();
            impWordsBrowser->setHtml(html);
        }
    }
    else { // for captions
        if(netReply->error() != QNetworkReply::NoError){
            displayStatusBarMessage("Some error occured while downloading subtitles",5000);
            captionDocument->clear();
            captionDocument->setHtml("<h3>Some error Occurred</h3>");
            netReply->abort();
        }
        else{
            statusBar->hide();
            QJsonDocument searchResults= QJsonDocument::fromJson(*networkDataBuffer);
            QJsonObject object = searchResults.array()[0].toObject(); // assumption that there is only one english subtitle for each video
            QDomDocument subtitles;
            subtitles.setContent(object.value("captions").toString().replace("<br />"," "));
            QString html("<head><style type='text/css'> "
                         "a {color: #903749; text-decoration: none;} </style></head><body>") ;

            //setting data to captionDocument
            //extracting timestamps and text
            QDomNodeList pTags = subtitles.elementsByTagName("p");
            dataToBeSent->clear();
            for(int i=0 ; i<pTags.count();++i){
                QString beginValue = pTags.at(i).toElement().attribute("begin");
                QString endValue = pTags.at(i).toElement().attribute("end");
                QString text = pTags.at(i).toElement().text();
                dataToBeSent->append(beginValue);
                html.append(QString("<p><a name=%1 href=#%1 begin=%1 end=%2>%3</a></p>").arg(beginValue).arg(endValue).arg(text));
            }
            QTimer::singleShot(5000,this,SLOT(sendDataPoints())); //to ensure that webengineWidget loads before data is sent
            captionDocument->clear();
            captionDocument->setHtml(html);

            QTextCursor cursor(captionDocument);
            normalCharFormat = cursor.charFormat();

            //extracting captionCursorPositions
            captionCursorPositions->clear();
            for (int i=0; i<pTags.count();++i) {
                QString beginValue = pTags.at(i).toElement().attribute("begin");
                captionCursorPositions->insert(beginValue,cursor.position());
                cursor.movePosition(QTextCursor::NextBlock);
            }

            //restoring some variables
            currentAnchor = "";
        }
    }

    networkDataBuffer->clear();
    netReply->deleteLater();
    return;
}

void study_nptel::startNotes(QJsonObject lectureInfo)
{
    int lectureNumber = lectureInfo.value("lecture_number").toInt();
    QString title = lectureInfo.value("title").toString();
    notesPlainTextEdit->setReadOnly(false);
    if(this->courseFolder == "NULL"){
        setCourseFolder();
    }
    this->lectureFolder=this->courseFolder + "/Lec " + QString::number(lectureNumber) + " " + title;
    QString temp = "Lec " + QString::number(lectureNumber) + " " + title;
    QDir folder(courseFolder);
    if(!folder.exists(lectureFolder)){
        if(!folder.mkdir(temp)){
            QMessageBox::critical(this,"ERROR","Unable to make lecture folder. Close and reopen this tab");
        }
    }

    //now we create a sequence file in this directory
    //sequence file tells the order in which the user saves the screenshots and notes.
    //during revision the user is shown these in the same order
    QFile file(lectureFolder+"/sequence.txt");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text )){
        QMessageBox::critical(this,"ERROR","Unable to create sequence file. Close and reopen this tab");
    }
    QTextStream out(&file);
    out<<"";
    file.close();
}

void study_nptel::enableMainIdeasDownloadPushButton()
{
    impWordsBrowser->clear();
    ui->mainIdeasDownloadPushButton->setDisabled(false);
}

void study_nptel::loadLecturesAndDownloads()
{
    //first we load lectures
    QJsonArray lectureArray = extraInfo.value("lectures").toArray();
    QJsonArray :: iterator p;
    for (p=lectureArray.begin();p!=lectureArray.end();++p) {
        lecture *lectureWidget = new lecture((*p).toObject(),netManager,this);
        lectureWidgets.append(lectureWidget);
        ui->lecturesScrollVerticalLayout->addWidget(lectureWidget,Qt::AlignBottom);

        connect(lectureWidget,&lecture::displayError,this,&study_nptel::displayStatusBarMessage);
        connect(lectureWidget,&lecture::changeVideo,this,&study_nptel::loadVideo);
        connect(lectureWidget,&lecture::changeVideo,this,&study_nptel::fetchCaptions);
        connect(lectureWidget,&lecture::changeVideo,this,&study_nptel::startNotes);
        connect(lectureWidget,&lecture::changeVideo,this,&study_nptel::changeLectureInfo);
        connect(lectureWidget,&lecture::changeVideo,this,&study_nptel::enableMainIdeasDownloadPushButton);
    }
}

void study_nptel::on_mainIdeasDownloadPushButton_clicked()
{
    ui->mainIdeasDownloadPushButton->setDisabled(true);
    //issue request for important words download
    searchRequest(QUrl(QString("%1/search/nptel/%2/%3/imp_words").arg(*host).arg(info.value("subjectid").toString())
                       .arg(currentLectureInfo.value("youtube_id").toString())));
}

void study_nptel::on_captionSearchLineEdit_returnPressed()
{
    if (captionQuery != ui->captionSearchLineEdit->text()) clearSearchInCaptions();
    pauseSync = true;
    pauseSyncTimer->start(5000);
    findInCaptions(ui->captionSearchLineEdit->text());
}

void study_nptel::on_captionFindNextPushButton_clicked()
{
    if (captionQuery != ui->captionSearchLineEdit->text()) clearSearchInCaptions();
    pauseSync = true;
    pauseSyncTimer->start(5000);
    findInCaptions(ui->captionSearchLineEdit->text());
}

void study_nptel::on_captionClearPushButton_clicked()
{
    clearSearchInCaptions();
}

void study_nptel::on_cleanNotesPushButton_clicked()
{

}

void study_nptel::on_clearNotesPushButton_clicked()
{
    if (*currentNotesFile == "NULL") notesPlainTextEdit->clear();
    else {
        if(!saveToFile(*currentNotesFile)) return;

        setCurrentNotesFile("NULL");
        notesPlainTextEdit->clear();

        displayStatusBarMessage("Notes Saved",5000);
    }
    return;
}

void study_nptel::on_openNotesPushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),lectureFolder,tr("Text Files (*.txt)"));
    if(fileName.isEmpty()) return;

    QString fileContent = readFile(fileName);
    if(fileContent == "ERROR") return;

    notesPlainTextEdit->clear();
    notesPlainTextEdit->setPlainText(fileContent);
}
