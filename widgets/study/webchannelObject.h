#ifndef WEBCHANNELOBJECT_H
#define WEBCHANNELOBJECT_H

#include <QObject>

/*
    An instance of this class gets published over the WebChannel and is then accessible to HTML clients.
*/
class webchannelObject : public QObject
{
    Q_OBJECT

public:
    webchannelObject(QObject *parent = nullptr){}

signals:
    void sendingDataPoints(QStringList);// This signal is emitted from the C++ side and recieved in webview.
    //it contains the timestamps in msec.

    void seekTo(QString timeString); // signal sent to html side. parameter is the time to seek to as a string.

    void changeSubtitles(QString anchor);// not used on the javscript side.

public slots:
    void emitChangeSubtitles(QString anchor){ //sent by html side. recieved by c++. indicates that new subtitles need to be highlighted.
        emit changeSubtitles(anchor);
    }
};


#endif // WEBCHANNELOBJECT_H
