#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QString *host, QNetworkAccessManager *netManager, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->netManager = netManager;

    setWindowIcon(QIcon("images/icon.jpg"));

    studyWidgets = new QMap<int,study *> ;
    searchWidgets = new QMap<int,course_searcher *>;
    reviseWidgets = new QMap<int,revision *>;
    lectureSelectionWidgets = new QMap<int,lectureSelection *>;

    //welcomeWidget is displayed first
    ui->tabWidget->setTabBarAutoHide(true);
    welcomeWidget= new welcome(this);
    ui->tabWidget->insertTab(0,welcomeWidget,"WELCOME");
    this->setAutoFillBackground(true);
    this->host=host;


    //connections
    connect(this,&MainWindow::studyTime,this,&MainWindow::studyTimeSlot);
    connect(ui->tabWidget,&QTabWidget::tabCloseRequested,this,&MainWindow::closeTab);
    connect(ui->tabWidget,&QTabWidget::currentChanged,this,&MainWindow::changeWindowTitle);
    connect(welcomeWidget,&welcome::searchTime,this,&MainWindow::createSearchWidget);
    connect(welcomeWidget,&welcome::studyTimeWelcome,this,&MainWindow::studyTimeSlot);
    connect(this,&MainWindow::reloadWelcome,welcomeWidget,&welcome::reloadRegisteredCourses);
    //first lecture for revision is selected then revision starts
    connect(welcomeWidget,&welcome::reviseTime,this,&MainWindow::lectureSelectionSlot);

}

MainWindow::~MainWindow()
{
    //deleting all studyWidgets
    QMap<int,study *> ::iterator p;
    for (p=studyWidgets->begin();p!=studyWidgets->end();++p){
        (*p)->deleteLater();
    }
    studyWidgets->clear();
    delete studyWidgets;

    //deleting all searchWidgets
    QMap<int,course_searcher *> ::iterator q;
    for (q=searchWidgets->begin();q!=searchWidgets->end();++q){
        (*q)->deleteLater();
    }
    searchWidgets->clear();
    delete searchWidgets;

    //deleting all reviseWidgets
    QMap<int,revision *> ::iterator t;
    for (t=reviseWidgets->begin();t!=reviseWidgets->end();++t){
        (*t)->deleteLater();
    }
    reviseWidgets->clear();
    delete reviseWidgets;

    //deleting all lectureSelectionWidgets
    QMap<int,lectureSelection *> ::iterator r;
    for (r=lectureSelectionWidgets->begin();r!=lectureSelectionWidgets->end();++r){
        (*r)->deleteLater();
    }
    lectureSelectionWidgets->clear();
    delete lectureSelectionWidgets;

    if(welcomeWidget != nullptr) welcomeWidget->deleteLater();

    delete ui;
}

void MainWindow::studyTimeSlot(QString courseName, QJsonObject *info, bool networkRequestNeeded)
{
    //creates a studyWidget and inserts it into the tabWidget
    study *studyWidget = nullptr;
    if (info->value("course_provider")=="NPTEL") studyWidget=new study_nptel(host,netManager,this,info,networkRequestNeeded);
    else if (info->value("course_provider")=="YOUTUBE")studyWidget=new study_youtube(host,netManager,this,info,networkRequestNeeded);

    studyWidgets->insert(ui->tabWidget->count(),studyWidget);
    ui->tabWidget->addTab(studyWidget,courseName);
    ui->tabWidget->setCurrentIndex(ui->tabWidget->count()-1);
    studyWidget->setIndex(ui->tabWidget->count()-1);

    //make connections
    connect(studyWidget,&study::reloadWelcome,this,&MainWindow::reloadWelcomeSlot);

    return;
}

void MainWindow::on_actionNew_Tab_triggered()
{
    createSearchWidget();
    return;
}

void MainWindow::closeTab(int index)
{
    ui->tabWidget->removeTab(index);
    if (studyWidgets->contains(index)) {
        studyWidgets->value(index)->deleteLater();
        studyWidgets->remove(index);
    }
    else if(searchWidgets->contains(index)){
        searchWidgets->value(index)->deleteLater();
        searchWidgets->remove(index);
    }
    else if(reviseWidgets->contains(index)){
        reviseWidgets->value(index)->deleteLater();
        reviseWidgets->remove(index);
    }
    else{
        lectureSelectionWidgets->value(index)->deleteLater();
        lectureSelectionWidgets->remove(index);
    }

    //updating all the indexes greater than 'index' because indexes of all tabs after deleted tab are decreased by one
    //first for searchwidgets
    QMap<int,course_searcher*>::iterator q;
    QMap<int,course_searcher*> temp;
    for (q=searchWidgets->begin();q!=searchWidgets->end();++q){
        if(q.key()>index) temp.insert(q.key()-1,q.value());
        else temp.insert(q.key(),q.value());
    }
    searchWidgets->swap(temp);

    //now for studyWidgets
    QMap<int,study *> :: iterator p;
    QMap<int,study*> anotherTemp;
    for (p=studyWidgets->begin();p!=studyWidgets->end();++p) {
        if(p.key()>index){
            (*p)->setIndex(p.key()-1);
            anotherTemp.insert(p.key()-1,p.value());
        }
        else anotherTemp.insert(p.key(),p.value());
    }
    studyWidgets->swap(anotherTemp);

    //then for reviseWidgets
    QMap<int,revision *> :: iterator t;
    QMap<int,revision*> yetAnotherTemp;
    for (t=reviseWidgets->begin();t!=reviseWidgets->end();++t) {
        if(t.key()>index)yetAnotherTemp.insert(t.key()-1,t.value());
        else yetAnotherTemp.insert(t.key(),t.value());
    }
    reviseWidgets->swap(yetAnotherTemp);

    //then lectureSelectionWidgets
    QMap<int,lectureSelection *> :: iterator r;
    QMap<int,lectureSelection*> yetAnotherrTemp;
    for (r=lectureSelectionWidgets->begin();r!=lectureSelectionWidgets->end();++r) {
        if(r.key()>index)yetAnotherrTemp.insert(r.key()-1,r.value());
        else yetAnotherrTemp.insert(r.key(),r.value());
    }
    lectureSelectionWidgets->swap(yetAnotherrTemp);

    return;
}

void MainWindow::on_actionScreenshot_triggered()
{
    QScreen *screen = QGuiApplication::primaryScreen();
    study *studyWidget= studyWidgets->value(ui->tabWidget->currentIndex());

    QPixmap screenshot = screen->grabWindow(studyWidget->getWebviewWinId(),0,0,studyWidget->getWebviewWidth(),studyWidget->getWebviewHeight());
    QString filename = "Screenshot" + QDateTime::currentDateTime().toString(Qt::ISODate);
    if(!studyWidget->writeToSequenceFile(filename+".jpeg")){
        QMessageBox::critical(this,"ERROR","Unable to write to sequence file");
        return;
    }
    if(!screenshot.save(*(studyWidget->getLectureFolder())+"/"+filename+".jpeg","jpeg",100)){
        QMessageBox::warning(this, tr("Save Error"), tr("The image could not be saved to \"%1\".").arg(QDir::toNativeSeparators(*(studyWidget->getLectureFolder()))));
    }
    else studyWidget->displayStatusBarMessage("Screenshot saved",5000);

}

void MainWindow::emitStudyTime(QJsonObject *info)
{
    emit studyTime(info->value("subjectname").toString(),info,true); //nptel only for the time being
    return;
}

void MainWindow::changeWindowTitle(int index)
{
    //setting the title simply as the tab bar title
    this->setWindowTitle(ui->tabWidget->tabText(index));
    return;
}

void MainWindow::changeTabTextSlot(QString text)
{
    ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),text);
    return;
}

void MainWindow::on_actionMake_Fullscreen_triggered()
{
    ui->tabWidget->tabBar()->hide();
    return;
}

void MainWindow::on_actionReturn_Normal_Size_triggered()
{
    ui->tabWidget->tabBar()->show();
    return;
}

void MainWindow::reviseTimeSlot(QJsonObject *courseInfo, QJsonObject *currentLectureInfo, QJsonArray *startedLecturesInfo)
{
    revision *reviseWidget = new revision(this,courseInfo,currentLectureInfo,startedLecturesInfo);
    reviseWidgets->insert(ui->tabWidget->count(), reviseWidget);
    reviseWidget->setIndex(ui->tabWidget->count());

    //check course type to decide what's to be displayed in the tab
    //first nptel
    if(courseInfo->value("course_provider").toString() == "NPTEL"){
        ui->tabWidget->addTab(reviseWidget, "REVISION: "+courseInfo->value("subjectname").toString());
    }

    //then Youtube

    ui->tabWidget->setCurrentIndex(ui->tabWidget->count()-1);

    //some connections
    connect(reviseWidget,&revision::reviseNext,this,&MainWindow::reviseTimeSlot);
}

void MainWindow::lectureSelectionSlot(QJsonObject *information)
{
    //check the type of course
    //first for nptel
    QJsonArray startedLectureInfo;
    lectureSelection *lectureSelectionWidget;
    if(information->value("course_provider").toString() == "NPTEL"){
        startedLectureInfo = getStartedLecturesNptel(information->value("subjectid").toString());
        if(startedLectureInfo.isEmpty()){
            QMessageBox::information(this,"No data Found","Seems like you haven't started studying ...");
            return;
        }
        lectureSelectionWidget = new lectureSelection(this, information, &startedLectureInfo);
    }
    //then for Youtube
    else if(information->value("course_provider").toString() == "YOUTUBE"){

    }
    lectureSelectionWidgets->insert(ui->tabWidget->count(),lectureSelectionWidget);

    //connections
    connect(lectureSelectionWidget,&lectureSelection::reviseTime,this,&MainWindow::reviseTimeSlot);

    //display the widget
    ui->tabWidget->addTab(lectureSelectionWidget,"Select a lecture");
    ui->tabWidget->setCurrentIndex(ui->tabWidget->count()-1);

    return;
}

void MainWindow::reloadWelcomeSlot()
{
    emit reloadWelcome();
}

void MainWindow::createSearchWidget()
{
    course_searcher *searchWidget=new course_searcher(host,netManager,this);
    searchWidgets->insert(ui->tabWidget->count(), searchWidget);
    ui->tabWidget->addTab(searchWidget,"Course Search");
    ui->tabWidget->setCurrentIndex(ui->tabWidget->count()-1);

    connect(searchWidget,&course_searcher::studyTime_courseSearcher,this,&MainWindow::emitStudyTime);
    connect(searchWidget,&course_searcher::changeTabText,this,&MainWindow::changeTabTextSlot);//to change tab text.
    connect(searchWidget,&course_searcher::changeTabText,this,&MainWindow::setWindowTitle);//to change Window title
}
