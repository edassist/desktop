#include "model.h"

bool checkDBExists(type t)
{
    if(t==nptel) {
        QFileInfo databaseFile (DBPATH+"/nptel.db");
        if(databaseFile.exists()) return true;

        //if not then create the database
        QSqlDatabase db;
        if(QSqlDatabase::contains("NPTEL"))db = QSqlDatabase::database("NPTEL");
        else if(createConnection(nptel)) db = QSqlDatabase::database("NPTEL");
        else return false;

        QSqlQuery query("",db);
        //courses table
        if(!query.exec("create table courses \
                    (disciplinename varchar(50) NOT NULL,\
                    subjectid varchar(10) NOT NULL PRIMARY KEY,\
                    subjectname varchar(200) NOT NULL,\
                    institute varchar(100) NOT NULL,\
                    medium varchar(20) NOT NULL,\
                    state bool NULL,\
                    extra_downloads bool NULL,\
                    coordinators varchar(150) NULL,\
                    creation_date datetime NOT NULL,\
                    course_path varchar(200) NULL);")) {
            qDebug()<<query.lastError()<<" "<<getLastedExecutedQuery(query);

            return false;
        }

        //lectures table
        //started determines whether the user has started that lecture or not
        if(!query.exec("create table lectures \
                    (subjectid varchar(10) NOT NULL,\
                    title varchar(300) NULL,\
                    youtube_id varchar(20) NULL,\
                    lecture_number int2 NOT NULL,\
                    video_download varchar(400) NULL,\
                    pdf_download varchar(400) NULL,\
                    started bool NOT NULL,\
                    CONSTRAINT lectures_pk PRIMARY KEY (subjectid, lecture_number),\
                    CONSTRAINT lectures_subjectid_fkey FOREIGN KEY (subjectid) REFERENCES courses(subjectid));")) {
            qDebug()<<query.lastError()<<" "<<getLastedExecutedQuery(query);

            return false;
        }
        //extra_downloads table
        if(!query.exec("create table extra_downloads \
                    (subjectid varchar(10) NOT NULL,\
                    download_link varchar(400) NOT NULL,\
                    module_name varchar(100) NULL,\
                    description varchar(400) NULL,\
                    CONSTRAINT extra_downloads_subjectid_fkey FOREIGN KEY (subjectid) REFERENCES courses(subjectid));")) {
            qDebug()<<query.lastError()<<" "<<getLastedExecutedQuery(query);

            return false;
        }

        return true;
    }

    return true; // for time being true for everythong else.

}

bool registerCourse(QString type, QJsonObject info)
{
    if(type == "NPTEL") {
        if(!checkDBExists(nptel)) return false;

        QSqlDatabase db;
        if(QSqlDatabase::contains("NPTEL")) db = QSqlDatabase::database("NPTEL");
        else if(createConnection(nptel)) db = QSqlDatabase::database("NPTEL");
        else return false;

        QSqlQuery insert("",db);
        //first for courses
        insert.prepare("INSERT INTO courses VALUES (:disciplinename,:subjectid,:subjectname,:institute,:medium,:state,\
                                       :extra_downloads,:coordinators,:creation_date,:course_path);");
        QJsonObject courseObject = info.value("courses").toObject();
        insert.bindValue(":subjectid",courseObject.value("subjectid").toString());
        insert.bindValue(":disciplinename",courseObject.value("disciplinename").toString());
        insert.bindValue(":subjectname",courseObject.value("subjectname").toString());
        insert.bindValue(":institute",courseObject.value("institute").toString());
        insert.bindValue(":medium",courseObject.value("medium").toString());
        insert.bindValue(":state",courseObject.value("state").toBool());
        insert.bindValue(":extra_downloads",courseObject.value("extra_downloads").toBool());
        insert.bindValue(":coordinators",courseObject.value("coordinators").toString());
        insert.bindValue(":creation_date",courseObject.value("creation_date").toString());
        insert.bindValue(":course_path",QVariant::String);
        if(!insert.exec()) {
            qDebug()<<"Unable to insert into courses table. "<<insert.lastError()<<" "<<getLastedExecutedQuery(insert);

            return false;
        }

        //then for lectures
        QJsonArray lectureArray = info.value("lectures").toArray();
        QJsonArray :: iterator p;
        for(p=lectureArray.begin(); p!=lectureArray.end(); ++p) {
            QJsonObject lectureObject = p->toObject();
            insert.prepare("INSERT INTO lectures VALUES (:subjectid,:title,:youtube_id,:lecture_number,:video_download,:pdf_download,:started);");
            insert.bindValue(":subjectid",lectureObject.value("subjectid_id").toString());//json key for subjectid is subjectid_id here
            insert.bindValue(":title",lectureObject.value("title").toString());
            insert.bindValue(":youtube_id",lectureObject.value("youtube_id").toString());
            insert.bindValue(":lecture_number",lectureObject.value("lecture_number").toInt());
            insert.bindValue(":video_download",lectureObject.value("video_download").toString());
            insert.bindValue(":pdf_download",lectureObject.value("pdf_download").toString());
            insert.bindValue(":started",false);
            if(!insert.exec()) {
                qDebug()<<"Unable to insert into lectures table "<<insert.lastError()<<" "<<getLastedExecutedQuery(insert);

                return false;
            }
        }

        //then for extra_downloads
        QJsonArray extraArray = info.value("extra_downloads").toArray();
        for(p=extraArray.begin(); p!=extraArray.end(); ++p) {
            QJsonObject extraObject = p->toObject();
            insert.prepare("INSERT INTO extra_downloads VALUES (:subjectid,:download_link,:module_name,:description);");
            insert.bindValue(":subjectid",extraObject.value("subjectid_id").toString());//json key for subjectid is subjectid_id here
            insert.bindValue(":download_link",extraObject.value("download_link").toString());
            insert.bindValue(":module_name",extraObject.value("module_name").toString());
            insert.bindValue(":description",extraObject.value("description").toString());
            if(!insert.exec()) {
                qDebug()<<"Unable to insert into extra_downloads table "<<insert.lastError()<<" "<<getLastedExecutedQuery(insert);

                return false;
            }
        }
        return true;

    }
    return true; //for the time being true for everything else
}

QString getLastedExecutedQuery(const QSqlQuery& query)
{
    /* helper function to see the query after the vaues have been bounded
    * taken from stackoverflow. user lightstep */
    QString str = query.lastQuery();
    QMapIterator<QString, QVariant> it(query.boundValues());
    while (it.hasNext())
    {
        it.next();
        str.replace(it.key(),it.value().toString());
    }
    return str;
}

QJsonObject getRegisteredCourses()
{
    QFileInfo npteldb(DBPATH+"/nptel.db");
    QFileInfo youtubedb(DBPATH+"/youtube.db");
    QJsonObject info;

    //for nptel
    if(npteldb.exists()) {
        QJsonObject temp;
        QJsonArray tempArray;
        bool chk = true;

        QSqlDatabase db;
        if(QSqlDatabase::contains("NPTEL")) db = QSqlDatabase::database("NPTEL");
        else if(createConnection(nptel)) db = QSqlDatabase::database("NPTEL");
        else {
            qDebug()<<"Unable to create a connection";
            chk = false;
        }
        if(chk) {
            QSqlQuery select("",db);
            if(!select.exec("SELECT * FROM courses ;")) {
                qDebug()<<"ERROR: "<<select.lastError().text()<<getLastedExecutedQuery(select);
            }
            else {
                while(select.next()) {
                    //for clarification see the CREATE DATABASE query for courses above
                    temp.insert("disciplinename",select.value(0).toString());
                    temp.insert("subjectid",select.value(1).toString());
                    temp.insert("subjectname",select.value(2).toString());
                    temp.insert("institute",select.value(3).toString());
                    temp.insert("medium",select.value(4).toString());
                    temp.insert("state",select.value(5).toBool());
                    temp.insert("extra_downloads",select.value(6).toBool());
                    temp.insert("coordinators",select.value(7).toString());
                    temp.insert("creation_date",select.value(8).toString());
                    temp.insert("course_path",select.value(9).toString());
                    tempArray.append(temp);
                }
                info.insert("NPTEL",tempArray);
            }
            select.finish();
        }
    }

    //for youtube
    if(youtubedb.exists()) {

    }

    return info;
}

QJsonArray getLectureNptel(QString subjectid)
{
    QJsonObject temp;
    QJsonArray lectureInfo;

    QSqlDatabase db;
    if(QSqlDatabase::contains("NPTEL")) db = QSqlDatabase::database("NPTEL");
    else if(createConnection(nptel)) {
        db = QSqlDatabase::database("NPTEL");
    }
    QSqlQuery select("",db);
    select.prepare("SELECT * FROM lectures WHERE subjectid = ?");
    select.addBindValue(subjectid);
    if(!select.exec()) {
        qDebug()<<"ERROR: "<<select.lastError().text()<<getLastedExecutedQuery(select);
    }
    else {
        while(select.next()) {
            //for clarification see the CREATE DATABASE query for lectures above
            temp.insert("subjectid_id",select.value(0).toString());
            temp.insert("title",select.value(1).toString());
            temp.insert("youtube_id",select.value(2).toString());
            temp.insert("lecture_number",select.value(3).toInt());
            temp.insert("video_download",select.value(4).toString());
            temp.insert("pdf_download",select.value(5).toString());
            lectureInfo.append(temp);
        }
    }


    return lectureInfo;
}

QJsonArray getExtraDowloadsNptel(QString subjectid)
{
    QJsonArray extraInfo;
    QJsonObject temp;
    QSqlDatabase db;
    if(QSqlDatabase::contains("NPTEL")) db = QSqlDatabase::database("NPTEL");
    else if(createConnection(nptel)) {
        db = QSqlDatabase::database("NPTEL");
    }
    QSqlQuery select("",db);
    select.prepare("SELECT * FROM extra_downloads WHERE subjectid = ?");
    select.addBindValue(subjectid);
    if(!select.exec()) {
        qDebug()<<"ERROR: "<<select.lastError().text()<<getLastedExecutedQuery(select);

    }
    else {
        while(select.next()) {
            //for clarification see the CREATE DATABASE query for extra_downloads above
            temp.insert("subjectid_id",select.value(0).toString());
            temp.insert("download_link",select.value(1).toString());
            temp.insert("module_name",select.value(2).toString());
            temp.insert("description",select.value(3).toString());
            extraInfo.append(temp);
        }
    }

    return extraInfo;

}

bool updateCoursePath(QString coursePath, QString subjectid)
{
    QSqlDatabase db;
    if(QSqlDatabase::contains("NPTEL"))db = QSqlDatabase::database("NPTEL");
    else if(createConnection(nptel)) {
        db = QSqlDatabase::database("NPTEL");
    }
    else return false;
    QSqlQuery update("",db);
    update.prepare("UPDATE courses SET course_path = :course_path WHERE subjectid = :subjectid");
    update.bindValue(":course_path",coursePath);
    update.bindValue(":subjectid",subjectid);
    if(!update.exec()) {
        qDebug()<<"ERROR: "<<update.lastError().text()<<" "<<getLastedExecutedQuery(update);

        return false;
    }

    return true;
}

bool removeCourseNptel(QString subjectid)
{
    QSqlDatabase db;
    if(QSqlDatabase::contains("NPTEL"))db = QSqlDatabase::database("NPTEL");
    else if(createConnection(nptel)) {
        db = QSqlDatabase::database("NPTEL");
    }
    else return false;

    QSqlQuery del("",db);
    //first courses table
    del.prepare("DELETE FROM courses WHERE subjectid = :subjectid");
    del.bindValue(":subjectid",subjectid);
    if(!del.exec()) {
        qDebug()<<"ERROR: "<<del.lastError().text()<<" "<<getLastedExecutedQuery(del);

        return false;
    }
    //lectures table
    del.prepare("DELETE FROM lectures WHERE subjectid = :subjectid");
    del.bindValue(":subjectid",subjectid);
    if(!del.exec()) {
        qDebug()<<"ERROR: "<<del.lastError().text()<<" "<<getLastedExecutedQuery(del);

        return false;
    }

    //extra_downloads
    del.prepare("DELETE FROM extra_downloads WHERE subjectid = :subjectid");
    del.bindValue(":subjectid",subjectid);
    if(!del.exec()) {
        qDebug()<<"ERROR: "<<del.lastError().text()<<" "<<getLastedExecutedQuery(del);

        return false;
    }


    return true;
}

bool createConnection(type t)
{
    if(t==nptel) {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE","NPTEL");
        db.setDatabaseName(DBPATH+"/nptel.db");
        if(!openConnection(db)) return false;
        else return true;
    }
    else {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE","YOUTUBE");
        db.setDatabaseName(DBPATH+"/youtube.db");
        if(!openConnection(db)) return false;
        else return true;
    }
}

bool openConnection(QSqlDatabase db)
{
    if(!db.open()) {
        qDebug()<<"ERROR: "<<db.lastError();
        QSqlDatabase::removeDatabase(db.connectionName());
        return false;
    }
    return true;
}

bool updateStartedNptel(QString subjectid, int lectureNumber)
{
    QSqlDatabase db;
    if(QSqlDatabase::contains("NPTEL"))db = QSqlDatabase::database("NPTEL");
    else if(createConnection(nptel)) {
        db = QSqlDatabase::database("NPTEL");
    }
    else return false;
    QSqlQuery update("",db);
    update.prepare("UPDATE lectures SET started = True WHERE subjectid = :subjectid AND lecture_number = :lecture_number");
    update.bindValue(":lecture_number",lectureNumber);
    update.bindValue(":subjectid",subjectid);
    if(!update.exec()) {
        qDebug()<<"ERROR: "<<update.lastError().text()<<" "<<getLastedExecutedQuery(update);

        return false;
    }

    return true;
}

QJsonArray getStartedLecturesNptel(QString subjectid)
{
    QJsonObject temp;
    QJsonArray startedLectureInfo;

    QSqlDatabase db;
    if(QSqlDatabase::contains("NPTEL")) db = QSqlDatabase::database("NPTEL");
    else if(createConnection(nptel)) {
        db = QSqlDatabase::database("NPTEL");
    }
    QSqlQuery select("",db);
    select.prepare("SELECT * FROM lectures WHERE subjectid = :subjectid AND started = True ORDER BY lecture_number");
    select.bindValue(":subjectid",subjectid);
    if(!select.exec()) {
        qDebug()<<"ERROR: "<<select.lastError().text()<<getLastedExecutedQuery(select);
    }
    else {
        while(select.next()) {
            //for clarification see the CREATE DATABASE query for lectures above
            temp.insert("subjectid_id",select.value(0).toString());
            temp.insert("title",select.value(1).toString());
            temp.insert("youtube_id",select.value(2).toString());
            temp.insert("lecture_number",select.value(3).toInt());
            temp.insert("video_download",select.value(4).toString());
            temp.insert("pdf_download",select.value(5).toString());
            startedLectureInfo.append(temp);
        }
    }


    return startedLectureInfo;
}
