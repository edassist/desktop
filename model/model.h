#ifndef MODEL_H
#define MODEL_H

/*this file provides all the functions needed by the application to
 * interact with the database. */

#include <QObject>
#include <QtSql>
#include <QDir>

//variables
/*the databases are to be stored where the application executable is stored.
 * caution when changing the directory of the application.
 * though at this point this doesn't happen anywhere in the code. */
static const QString DBPATH = QDir::currentPath();

/* four types of database exist
 * one for nptel courses
 * one for youtube courses
 * one for user data */
enum type : int {user, nptel, youtube};

//functions
bool checkDBExists(type);//check if database exists, if not create it.

//for course related databases
bool registerCourse(QString ,QJsonObject);//creates a row for a course that user starts
bool updateCoursePath(QString ,QString);//for the time being only for nptel courses
QString getLastedExecutedQuery(const QSqlQuery& );//see function definition
QJsonObject getRegisteredCourses();//returns info about all the registered courses
bool createConnection(type t);
bool openConnection(QSqlDatabase db);

//nptel specific
QJsonArray getLectureNptel(QString subjectid);//returns lecture info for NPTEL courses
QJsonArray getExtraDowloadsNptel(QString subjectid);//returns extra_downloads info for NPTEL courses
bool removeCourseNptel(QString subjectid);//removes the course from all three tables
bool updateStartedNptel(QString subjectid, int lectureNumber);//updates the started attribute in lectures table. Sets started to True
QJsonArray getStartedLecturesNptel(QString subjectid);//gets all lectures which have been started

#endif // MODEL_H
