#-------------------------------------------------
#
# Project created by QtCreator 2019-07-17T19:54:03
#
#-------------------------------------------------

QT += core gui network xml sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webengine webenginewidgets webchannel websockets

TARGET = Desktop
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

OBJECTS_DIR = objects-Desktop
MOC_DIR = moc-Desktop

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        model/model.cpp \
        widgets/course_searcher/course_searcher.cpp \
        widgets/course_searcher/filtersdialog/filtersdialog.cpp \
        widgets/course_searcher/filtersdialog/filtersdialog_mitocw.cpp \
        widgets/course_searcher/filtersdialog/filtersdialog_nptel.cpp \
        widgets/course_searcher/filtersdialog/filtersdialog_udacity.cpp \
        widgets/course_searcher/learn_more/learn_more.cpp \
        widgets/course_searcher/learn_more/learn_more_mitocw.cpp \
        widgets/course_searcher/learn_more/learn_more_nptel.cpp \
        widgets/course_searcher/learn_more/learn_more_udacity.cpp \
        widgets/revision/lectureselection/lectureselection.cpp \
        widgets/revision/revision.cpp \
        widgets/search_result/search_result.cpp \
        widgets/study/lecture/lecture.cpp \
        widgets/study/loadingdialog/loadingdialog.cpp \
        widgets/study/study.cpp \
        widgets/study/study_nptel.cpp \
        widgets/study/study_youtube.cpp \
        widgets/study/wheel_event_filter.cpp \
        widgets/welcome/welcome.cpp

HEADERS += \
        mainwindow.h \
        model/model.h \
        widgets/course_searcher/course_searcher.h \
        widgets/course_searcher/filtersdialog/filtersdialog.h \
        widgets/course_searcher/filtersdialog/filtersdialog_mitocw.h \
        widgets/course_searcher/filtersdialog/filtersdialog_nptel.h \
        widgets/course_searcher/filtersdialog/filtersdialog_udacity.h \
        widgets/course_searcher/learn_more/learn_more.h \
        widgets/course_searcher/learn_more/learn_more_mitocw.h \
        widgets/course_searcher/learn_more/learn_more_nptel.h \
        widgets/course_searcher/learn_more/learn_more_udacity.h \
        widgets/revision/lectureselection/lectureselection.h \
        widgets/revision/revision.h \
        widgets/search_result/search_result.h \
        widgets/study/lecture/lecture.h \
        widgets/study/loadingdialog/loadingdialog.h \
        widgets/study/study.h \
        widgets/study/study_nptel.h \
        widgets/study/study_youtube.h \
        widgets/study/webchannelObject.h \
        widgets/study/wheel_event_filter.h \
        widgets/welcome/welcome.h

FORMS += \
        mainwindow.ui \
        widgets/course_searcher/course_searcher.ui \
        widgets/course_searcher/filtersdialog/filtersdialog.ui \
        widgets/course_searcher/filtersdialog/filtersdialog_mitocw.ui \
        widgets/course_searcher/filtersdialog/filtersdialog_nptel.ui \
        widgets/course_searcher/filtersdialog/filtersdialog_udacity.ui \
        widgets/course_searcher/learn_more/learn_more_mitocw.ui \
        widgets/course_searcher/learn_more/learn_more_nptel.ui \
        widgets/course_searcher/learn_more/learn_more_udacity.ui \
        widgets/revision/lectureselection/lectureselection.ui \
        widgets/revision/revision.ui \
        widgets/search_result/search_result.ui \
        widgets/study/lecture/lecture.ui \
        widgets/study/loadingdialog/loadingdialog.ui \
        widgets/study/study_nptel.ui \
        widgets/study/study_youtube.ui \
        widgets/welcome/welcome.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc


