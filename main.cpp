#include "mainwindow.h"
#include <QApplication>
#include<QString>
#include<QFile>
#include<QTextStream>
#include <QNetworkAccessManager>

QString readTextFile(QString path)
{
    QFile file(path);

    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        return in.readAll();
    }

    return "";
}

int main(int argc, char *argv[])
{
    QString host("http://edassist.mightymaharaja.tk");//ip/name of the remote host.
    QNetworkAccessManager netManager;

    QApplication a(argc, argv);
    QString css = readTextFile(":/styles/style.css");

    if( css.length() > 0)
    {
        a.setStyleSheet(css);
    }
    MainWindow w(&host,&netManager,nullptr);
    w.show();

    return a.exec();
}
