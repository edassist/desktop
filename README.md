# EdAssist

A cross platform desktop application to make learning through YouTube more structured.

To test out the application you can download either the [Windows Build](https://drive.google.com/file/d/147geakIrXJPLgaaR3CUFWg8YS72m9L6i/view?usp=sharing) or the [Linux Build](https://drive.google.com/file/d/1j8IxX6K9PKYOxRyejE7ffJgftcP6gomm/view?usp=sharing). Before testing out the application, watch this [demo](https://youtu.be/8nei9aZ69o8) and read till the end.

## UI Tour
                                                
| Explanation | Image |
| ------ | ------ |
| Welcome Page | ![Welcome Page](/images/welcome_page.png) |
| Searching for courses | ![Searching Courses](/images/course_search.png) |
| Filter Dialog | ![Filter Dialog](/images/filters.png) |
| Using filter dialog to find courses | ![Filter's use](/images/filters2.png) |
| Lecture Playlist | ![Lectures Playlist](/images/lecture_playlist.png) |
| Note Saving Widget | ![Note Saving Widget](/images/notes_widget.png) |
| Study Section | ![Study Section](/images/study_section.png) |
| Revision Section | ![Revision Section](/images/revision_section.png) |

## BUGS
You will encounter the following bugs while using the application.

On Windows it is unable to create any files so you would receive errors like:

![Windows Error 1](/images/windows_error_1.png) 

![Windows Error 2](/images/windows_error_2.png)

![Windows Error 3](/images/windows_error_3.png)

On Linux it is not able to display the YouTube video, so the application will look like this :

![Windows Error 1](/images/linux_error_1.png)

These will be rectified in the future. The aim is to make the user experience similar to what was shown in the demo above.